//
//  GADesignableButton.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

   @IBDesignable
   open class  GADesignableButton: UIButton {
       
       override init(frame: CGRect) {
           super.init(frame: frame)
           setup()
       }
       required public init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           setup()
       }
       
       func setup() {
           
           layer.cornerRadius = 3
           layer.shadowColor = UIColor.black.cgColor
           layer.shadowOffset = CGSize(width: 0.6, height: 0.6)
           layer.shadowOpacity = 0.4
           layer.shadowRadius = 2.0
           layer.masksToBounds = false
       }
}
