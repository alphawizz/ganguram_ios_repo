//
//  GAExtensions.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {

func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let result = emailTest.evaluate(with: testStr)
    return result
}
}

//struct UserProfileData {
//    static let key = "userProfileCache"
//    static func save(_ value: UserInformation!) {
//        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: key)
//    }
//
//    static func get() -> UserInformation! {
//        var userData: UserInformation!
//        if let data = UserDefaults.standard.value(forKey: key) as? Data {
//            userData = try? PropertyListDecoder().decode(UserInformation.self, from: data)
//            return userData!
//        } else {
//            return userData
//        }
//    }
//    static func remove() {
//        UserDefaults.standard.removeObject(forKey: key)
//    }
//}
//
