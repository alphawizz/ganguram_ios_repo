//
//  GADesignableView.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable

open class GADesignableView: UIView {
        
        // Only override draw() if you perform custom drawing.
        // An empty implementation adversely affects performance during animation.
    override open func draw(_ rect: CGRect) {
            // Drawing code
            
            setProperties()
        }

        func setProperties()  {
          // self.layer.shadowColor = UIC
            self.layer.shadowOpacity = 0.5
           self.layer.shadowOffset = .zero
           self.layer.shadowRadius = 2
            self.layer.cornerRadius = 3
        }
    
}
