//
//  GAAddressTableViewCell.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import KRProgressHUD

class GAAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var labelAddress1: UILabel!
    @IBOutlet weak var labelAddress2: UILabel!
    
    @IBOutlet weak var buttonEdit: GADesignableButton!
    @IBOutlet weak var buttonDelete: GADesignableButton!
    @IBOutlet weak var buttonSave: GADesignableButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension GAProfileViewController: UITableViewDelegate, UITableViewDataSource {

 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if GAShippingAddressList.shared.arrayShippingAddress.count == 0 {
        return 1
    } else {
    return GAShippingAddressList.shared.arrayShippingAddress.count
    }
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
      let orderCell = tableviewAddress.dequeueReusableCell(withIdentifier: String(describing: GAAddressTableViewCell.self)) as! GAAddressTableViewCell
    
    let emptyCell = tableviewAddress.dequeueReusableCell(withIdentifier: String(describing: GANoDataTableviewCellTableViewCell.self)) as! GANoDataTableviewCellTableViewCell
    
    if GAShippingAddressList.shared.arrayShippingAddress.count == 0 {
        
        emptyCell.labelMessage.text = "No recent address available"
        return emptyCell
    }
    
    let address = GAShippingAddressList.shared.arrayShippingAddress[indexPath.row]
    
    orderCell.labelAddress1.text = address.Address1
    orderCell.labelAddress2.text = "\(address.Address2!), \(address.State!), \( address.City!), \(address.Pincode!)"
    
    orderCell.buttonDelete.tag = indexPath.row
    orderCell.buttonEdit.tag = indexPath.row
    orderCell.buttonSave.tag = indexPath.row
    
    if addressID == address.id! {
        orderCell.buttonSave.backgroundColor = GAColors.themeButtonColor
        orderCell.buttonSave.setTitleColor(UIColor.white, for: .normal)
        orderCell.buttonSave.setTitle("Saved", for: .normal)
    } else {
        orderCell.buttonSave.backgroundColor = .white
        orderCell.buttonSave.setTitleColor(UIColor.darkGray, for: .normal)
        orderCell.buttonSave.setTitle("Save", for: .normal)

    }
    
    orderCell.buttonDelete.addTarget(self, action: #selector(self.onDelete(buttonDelete:)), for: .touchUpInside)
    orderCell.buttonEdit.addTarget(self, action: #selector(self.onEdit(buttonEdit:)), for: .touchUpInside)
    orderCell.buttonSave.addTarget(self, action: #selector(self.onSave(buttonSave:)), for: .touchUpInside)

    return orderCell
 }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func onDelete(buttonDelete : UIButton)  {
        
        let address = GAShippingAddressList.shared.arrayShippingAddress[buttonDelete.tag]
        callDeleteAddress(addressData: address)
    }
    
    @objc func onEdit(buttonEdit : UIButton)  {
        self.performSegue(withIdentifier: String.init(describing: GAAddAddressViewController.self), sender: buttonEdit.tag)
    }
    
    @objc func onSave(buttonSave : UIButton)  {
        
        let address = GAShippingAddressList.shared.arrayShippingAddress[buttonSave.tag]
        
        let encoder = JSONEncoder()
                   if let encoded = try? encoder.encode(address){
                       UserDefaults.standard.set(encoded, forKey: GAUserKey.SavedAddress)
                    addressID = address.id!
                   }
        
        tableviewAddress.reloadData()
    }
    
    func getSavedAddress()  {
        
        if let objects = UserDefaults.standard.value(forKey: GAUserKey.SavedAddress) as? Data {
                         let decoder = JSONDecoder()
                         if let cardList = try? decoder.decode(ShippingAddress.self, from: objects) as ShippingAddress {
                            
                            addressID = cardList.id!
            }
        }
    }
    
    func callDeleteAddress(addressData : ShippingAddress)  {
        
       if Connectivity.isConnectedToInternet() {
        
        let dictData : [String : Any] = ["address_id" : addressData.id!]
        
            KRProgressHUD.show()
            
            GAService.shared.apiForDeleteAddress(param: dictData) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    
                    let index = GAShippingAddressList.shared.arrayShippingAddress.firstIndex(where: {($0.id) == addressData.id})
                    
                    GAShippingAddressList.shared.arrayShippingAddress.remove(at: index!)
                    
                    if self.addressID == addressData.id! {
                        UserDefaults.standard.removeObject(forKey: GAUserKey.SavedAddress)
                    }
                    
                    self.tableviewAddress.reloadData()
                    
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "GAAddAddressViewController" {
            
            let vc = segue.destination as! GAAddAddressViewController
            vc.addressDetail = GAShippingAddressList.shared.arrayShippingAddress[sender as! Int]
            vc.isFromEditAddress = true
        }
    }
}
