//
//  GACardTableviewCell.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage

class GACardTableviewCell: UITableViewCell {

    @IBOutlet weak var labelTotalPrice: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var imageviewProduct: UIImageView!
    @IBOutlet weak var labelProductName: UILabel!
    @IBOutlet weak var buttonDelete: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var buttonMinus: UIButton!
    
    @IBOutlet weak var labelCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension GACardListViewController: UITableViewDelegate, UITableViewDataSource {

 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if productList == nil {
        return 1
    } else {
        return productList.count
    }
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
      let orderCell = tableviewList.dequeueReusableCell(withIdentifier: String(describing: GACardTableviewCell.self)) as! GACardTableviewCell
    
    let emptyCell = tableviewList.dequeueReusableCell(withIdentifier: String(describing: GANoDataTableviewCellTableViewCell.self)) as! GANoDataTableviewCellTableViewCell
    
    if productList == nil {
        emptyCell.labelMessage.text = "Your card is empty !!"
        return emptyCell
    }
    
    let dict = productList[indexPath.row]
    orderCell.labelProductName.text = dict.productName
    orderCell.labelPrice.text = "\(dict.price!)rs"
    
    orderCell.labelCount.text = "\(dict.productCount ?? 1)"
    
    let intPrice = Int(dict.price!)
    
    
    let totalCount = intPrice! * dict.productCount!
    orderCell.labelTotalPrice.text = "\(totalCount)rs"
    
    orderCell.imageviewProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
           orderCell.imageviewProduct.sd_setImage(with: URL.init(string: dict.productImage!)) { (image, error, cache, urls) in
               if (error != nil) {
                   // Failed to load image
                  orderCell.imageviewProduct.image = UIImage(named: "imageload")
               } else {
                   // Successful in loading image
                   orderCell.imageviewProduct.image = image
               }
           }
    
    orderCell.buttonPlus.tag = indexPath.row
    orderCell.buttonMinus.tag = indexPath.row
    orderCell.buttonDelete.tag = indexPath.row
    
    orderCell.buttonPlus.addTarget(self, action: #selector(self.onPlus(buttonPlus:)), for: .touchUpInside)
    orderCell.buttonMinus.addTarget(self, action: #selector(self.onMinus(buttonMinus:)), for: .touchUpInside)
    orderCell.buttonDelete.addTarget(self, action: #selector(self.onDelete(buttonDelete:)), for: .touchUpInside)

    return orderCell
 }
    
    @objc func onPlus(buttonPlus : UIButton)  {
        
        let dict = productList[buttonPlus.tag]
        
        let totalCount = dict.productCount! + 1
        
        let intPrice = Int(dict.price!)
        let totalPrice = intPrice! * dict.productCount!
        
        let indexPath = IndexPath.init(row: buttonPlus.tag, section: 0)
        
        let cell = tableviewList.cellForRow(at: indexPath) as! GACardTableviewCell
        cell.labelTotalPrice.text = "\(totalPrice)"
        cell.labelCount.text = "\(totalCount)"
        
        let index = productList.firstIndex(where: {($0.productId) == dict.productId})
        dict.productCount = totalCount
        
        productList.remove(at: index!)
        productList.insert(dict, at: index!)
        
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(productList){
           UserDefaults.standard.set(encoded, forKey: GAUserKey.AddToCard)
        }
        
       let currentPrice = Int(dict.price!)
        let formattedString = self.labelSubtotal.text!.replacingOccurrences(of: "rs", with: "")
        let intGlobalPrice =  Int(formattedString)
        
        let priceValue = intGlobalPrice! + currentPrice!

        self.labelSubtotal.text = "\(priceValue)rs"
        self.labelShipping.text = "20rs"
        self.labelCoupanApplied.text = "0rs"
        self.labelTotal.text = "\(priceValue + 20)rs"
        
        tableviewList.reloadData()
    }
    
    @objc func onMinus(buttonMinus : UIButton)  {
        
        let dict = productList[buttonMinus.tag]
        
        if dict.productCount! > 1 {
            let totalCount = dict.productCount! - 1
            
            let intPrice = Int(dict.price!)
            let totalPrice = intPrice! * dict.productCount!
            
            let indexPath = IndexPath.init(row: buttonMinus.tag, section: 0)
            
            let cell = tableviewList.cellForRow(at: indexPath) as! GACardTableviewCell
            cell.labelTotalPrice.text = "\(totalPrice)"
            cell.labelCount.text = "\(totalCount)"
            
            let index = productList.firstIndex(where: {($0.productId) == dict.productId})
            dict.productCount = totalCount
            
            productList.remove(at: index!)
            productList.insert(dict, at: index!)
            
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(productList){
                UserDefaults.standard.set(encoded, forKey: GAUserKey.AddToCard)
            }
           
            let currentPrice = Int(dict.price!)
            let formattedString = self.labelSubtotal.text!.replacingOccurrences(of: "rs", with: "")
            let intGlobalPrice =  Int(formattedString)
            
            let priceValue = intGlobalPrice! - currentPrice!
            
            self.labelSubtotal.text = "\(priceValue)rs"
            self.labelShipping.text = "20rs"
            self.labelCoupanApplied.text = "0rs"
            self.labelTotal.text = "\(priceValue + 20)rs"
            tableviewList.reloadData()
        }
       }
    
    @objc func onDelete(buttonDelete : UIButton)  {
        
        let dict = productList[buttonDelete.tag]
        let index = productList.firstIndex(where: {($0.productId) == dict.productId})
        productList.remove(at: index!)
        
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(productList){
            UserDefaults.standard.set(encoded, forKey: GAUserKey.AddToCard)
        }
        
        let formattedString = self.labelSubtotal.text!.replacingOccurrences(of: "rs", with: "")
        
        let intTotalPrice =  Int(formattedString)
        let currentPrice = Int(dict.price!)
        
        let quantty = currentPrice! * dict.productCount!
        let priceValue = intTotalPrice! - quantty
        
        if productList.count == 0 {
            self.labelSubtotal.text = "0"
            self.labelShipping.text = "0"
            self.labelCoupanApplied.text = "0"
            self.labelTotal.text = "0"
            
        } else {
            self.labelSubtotal.text = "\(priceValue)rs"
            self.labelShipping.text = "20rs"
            self.labelCoupanApplied.text = "0rs"
            self.labelTotal.text = "\(priceValue + 20)rs"
        }
        
        self.view.makeToast("Product removed successfully!!")
        
        tableviewList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 133
    }
}
