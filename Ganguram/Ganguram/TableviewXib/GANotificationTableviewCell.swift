//
//  GANotificationTableviewCell.swift
//  Ganguram
//
//  Created by Apple on 25/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class GANotificationTableviewCell: UITableViewCell {

    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelPaymentStatus: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension GANotificationListViewController: UITableViewDelegate, UITableViewDataSource {

 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 10
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
      let orderCell = tableviewNotificationList.dequeueReusableCell(withIdentifier: String(describing: GANotificationTableviewCell.self)) as! GANotificationTableviewCell
    return orderCell
 }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
    }
}
