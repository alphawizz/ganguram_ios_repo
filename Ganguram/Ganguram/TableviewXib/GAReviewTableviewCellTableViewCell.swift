//
//  GAReviewTableviewCellTableViewCell.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class GAReviewTableviewCellTableViewCell: UITableViewCell {

    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelComment: UILabel!
    @IBOutlet weak var viewReating: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension GAProductDetailViewController: UITableViewDelegate, UITableViewDataSource {

 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if GAReviewList.shared.arrayReviewData.count == 0 {
        return 1
    } else {
        return GAReviewList.shared.arrayReviewData.count

    }
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
      let orderCell = tableviewReviewList.dequeueReusableCell(withIdentifier: String(describing: GAReviewTableviewCellTableViewCell.self)) as! GAReviewTableviewCellTableViewCell
    
    let emptyCell = tableviewReviewList.dequeueReusableCell(withIdentifier: String(describing: GANoDataTableviewCellTableViewCell.self)) as! GANoDataTableviewCellTableViewCell
    
    if GAReviewList.shared.arrayReviewData.count == 0 {
        
        emptyCell.labelMessage.text = "No reviews found !!"
        return emptyCell
    }
    
    let data = GAReviewList.shared.arrayReviewData[indexPath.row]
    orderCell.labelComment.text = data.comment
    orderCell.viewReating.rating = Double(data.rating!)
    orderCell.labelDate.text = data.time
    orderCell.labelUserName.text = data.user_fullname
    
    return orderCell
 }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
