//
//  GAPlaceOrderTableviewCell.swift
//  Ganguram
//
//  Created by Apple on 21/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage

class GAPlaceOrderTableviewCell: UITableViewCell {

    @IBOutlet weak var labelProductName: UILabel!
    @IBOutlet weak var labelQuantity: UILabel!
    @IBOutlet weak var labelSingleProductPrice: UILabel!
    
    @IBOutlet weak var imageviewProduct: UIImageView!
    @IBOutlet weak var labelTotalPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension GAPlacedOrderListViewController: UITableViewDelegate, UITableViewDataSource {

 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return productList.count
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
      let orderCell = tableviewPlacedOrder.dequeueReusableCell(withIdentifier: String(describing: GAPlaceOrderTableviewCell.self)) as! GAPlaceOrderTableviewCell
    
    let dictProduct = productList[indexPath.row]
    orderCell.labelProductName.text = dictProduct.productName
    orderCell.labelQuantity.text = "Qty : \(dictProduct.productCount!)"
    orderCell.labelSingleProductPrice.text = "\(dictProduct.price!)rs"
    
    let total = dictProduct.productCount! * Int(dictProduct.price!)!
    orderCell.labelTotalPrice.text = "\(total)rs"
    
    orderCell.imageviewProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
    orderCell.imageviewProduct.sd_setImage(with: URL.init(string: dictProduct.productImage!)) { (image, error, cache, urls) in
        if (error != nil) {
            // Failed to load image
            orderCell.imageviewProduct.image = UIImage(named: "imageload")
        } else {
            // Successful in loading image
            orderCell.imageviewProduct.image = image
        }
    }
    
    return orderCell
 }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
}


extension GAInvoiceDetailViewController: UITableViewDelegate, UITableViewDataSource {

 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return GAInvoiceProducts.shared.arrayInvoiceProductList.count
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
      let orderCell = tableviewProductList.dequeueReusableCell(withIdentifier: String(describing: GAPlaceOrderTableviewCell.self)) as! GAPlaceOrderTableviewCell
    
    let dictProduct = GAInvoiceProducts.shared.arrayInvoiceProductList[indexPath.row]
    orderCell.labelProductName.text = dictProduct.product_name
    orderCell.labelQuantity.text = "Qty : \(dictProduct.productQuantity!)"
    orderCell.labelSingleProductPrice.text = "\(dictProduct.price!)rs"
    orderCell.labelTotalPrice.text = "\(dictProduct.totalPrice!)rs"
    
    orderCell.imageviewProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
    orderCell.imageviewProduct.sd_setImage(with: URL.init(string: dictProduct.productImage!)) { (image, error, cache, urls) in
        if (error != nil) {
            // Failed to load image
            orderCell.imageviewProduct.image = UIImage(named: "imageload")
        } else {
            // Successful in loading image
            orderCell.imageviewProduct.image = image
        }
    }
    
    return orderCell
 }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
}
