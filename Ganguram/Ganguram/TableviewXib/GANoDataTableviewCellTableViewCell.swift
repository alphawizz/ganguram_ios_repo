//
//  GANoDataTableviewCellTableViewCell.swift
//  Ganguram
//
//  Created by Apple on 20/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class GANoDataTableviewCellTableViewCell: UITableViewCell {

    @IBOutlet weak var labelMessage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
