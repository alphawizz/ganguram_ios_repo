//
//  GAMyOrderTableviewCell.swift
//  Ganguram
//
//  Created by Apple on 11/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class GAMyOrderTableviewCell: UITableViewCell {

    @IBOutlet weak var labelInvoiceNumber: UILabel!
    @IBOutlet weak var labelProductPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension GAMyOrderViewController: UITableViewDelegate, UITableViewDataSource {

 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return GAInvoice.shared.arrayInvoiceList.count
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
      let orderCell = tableMyOrder.dequeueReusableCell(withIdentifier: String(describing: GAMyOrderTableviewCell.self)) as! GAMyOrderTableviewCell
    
    let dict = GAInvoice.shared.arrayInvoiceList[indexPath.row]
    orderCell.labelProductPrice.text = "Total Amount : \(dict.totalPrice!)"
    orderCell.labelInvoiceNumber.text = "Invoice Number : \(dict.invoice_Id!)"
    
    return orderCell
 }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "GAInvoiceDetailViewController", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "GAInvoiceDetailViewController" {
            
            let vc = segue.destination as! GAInvoiceDetailViewController
            let dict = GAInvoice.shared.arrayInvoiceList[sender as! Int]
            vc.selectedInvoiceID = dict.invoice_Id!
            vc.invoiceDetail = dict
        }
    }
}
