//
//  GAWishListProductCollectionview.swift
//  Ganguram
//
//  Created by Apple on 16/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import KRProgressHUD

class GAWishListProductCollectionview: UICollectionViewCell {

    @IBOutlet weak var imageviewWishedProduct: UIImageView!
    
    @IBOutlet weak var viewRating: FloatRatingView!
    @IBOutlet weak var labelProductName: UILabel!
    @IBOutlet weak var buttonDeleteWish: UIButton!
    @IBOutlet weak var labelProductPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}


// Mark -> Form wishlist screen

extension GAWishListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if GAWishedProduct.shared.arrayWishProducts.count == 0 {
            return 1
        } else {
             return GAWishedProduct.shared.arrayWishProducts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionviewWishList.dequeueReusableCell(withReuseIdentifier: String(describing: GAWishListProductCollectionview.self), for: indexPath) as! GAWishListProductCollectionview
        
        if GAWishedProduct.shared.arrayWishProducts.count == 0 {
            
        let noDataCell = collectionviewWishList.dequeueReusableCell(withReuseIdentifier: String(describing: GANoDataCollectionViewCell.self), for: indexPath) as! GANoDataCollectionViewCell
            return noDataCell
        }
        
        let productDetail = GAWishedProduct.shared.arrayWishProducts[indexPath.item]
        
        cell.labelProductName.text = productDetail.product_name
        cell.viewRating.rating = Double(productDetail.rating!)
        
        cell.imageviewWishedProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imageviewWishedProduct.sd_setImage(with: URL.init(string: productDetail.product_image!)) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell.imageviewWishedProduct.image = UIImage(named: "imageload")
            } else {
                // Successful in loading image
                cell.imageviewWishedProduct.image = image
            }
        }
        
        cell.buttonDeleteWish.tag = indexPath.item
        
        cell.buttonDeleteWish.addTarget(self, action: #selector(self.onRemoveFromWishList(buttonRemove:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if GAWishedProduct.shared.arrayWishProducts.count == 0 {
        return CGSize(width: collectionView.frame.size.width, height: 244)
        } else {
            return CGSize(width: collectionView.frame.size.width/2, height: 244)
        }
    }
    
    @objc func onRemoveFromWishList(buttonRemove : UIButton) {
        
        let productDetail = GAWishedProduct.shared.arrayWishProducts[buttonRemove.tag]
        callApiForDeleteProductFromWishlist(productWishList: productDetail)
    }
    
    func callApiForDeleteProductFromWishlist(productWishList : wishProducDetail)  {
        
        if Connectivity.isConnectedToInternet() {
            
            KRProgressHUD.show()
            
            let dictData : [String : Any] = ["product_id" : productWishList.product_id!,
                                             "user_id" : GAUser.shared.loginInfoObj.userId!]
            
            GAService.shared.apiForRemoveWishList(param: dictData) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    
                    let index = GAWishedProduct.shared.arrayWishProducts.firstIndex(where: {($0.product_id) == productWishList.product_id!})
                    
                    GAWishedProduct.shared.arrayWishProducts.remove(at: index!)
                    
                    self.view.makeToast("Product removed from wishlist")
                    self.collectionviewWishList.reloadData()
                    
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
}
