//
//  GAProductWithOnlyNameCollectionViewCell.swift
//  Ganguram
//
//  Created by Apple on 11/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage

class GAProductWithOnlyNameCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageviewCategory: UIImageView!
    @IBOutlet weak var labelCategoryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension GAProductListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return GACategory.shared.arrayCategoryDeatil.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionviewProductList.dequeueReusableCell(withReuseIdentifier: String(describing: GAProductWithOnlyNameCollectionViewCell.self), for: indexPath) as! GAProductWithOnlyNameCollectionViewCell
        
        let categorydata = GACategory.shared.arrayCategoryDeatil[indexPath.row]
        cell.labelCategoryName.text = categorydata.title
        
        cell.imageviewCategory.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imageviewCategory.sd_setImage(with: URL.init(string: categorydata.image!)) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell.imageviewCategory.image = UIImage(named: "imageload")
            } else {
                // Successful in loading image
                cell.imageviewCategory.image = image
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/2, height: 210)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: String.init(describing: GACategoryProductsViewController.self), sender: indexPath.row)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "GACategoryProductsViewController" {
            
            let vc = segue.destination as! GACategoryProductsViewController
            vc.selctedCategoryDetail = GACategory.shared.arrayCategoryDeatil[sender as! Int]
        }
    }
    
}
