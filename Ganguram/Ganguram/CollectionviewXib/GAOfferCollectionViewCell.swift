//
//  GAOfferCollectionViewCell.swift
//  Ganguram
//
//  Created by Apple on 16/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage

class GAOfferCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelOffLabel: UILabel!
    @IBOutlet weak var labelOldPrice: UILabel!
    @IBOutlet weak var labelNewProduct: UILabel!
    @IBOutlet weak var labelProductName: UILabel!
    
    @IBOutlet weak var imageviewProduct: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension GAOfferViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return GAOffer.shared.arrayOfferDeatil.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionviewOffer.dequeueReusableCell(withReuseIdentifier: String(describing: GAOfferCollectionViewCell.self), for: indexPath) as! GAOfferCollectionViewCell
        
        let productDetail = GAOffer.shared.arrayOfferDeatil[indexPath.row]
        
        cell.labelProductName.text = productDetail.product_name
        cell.labelNewProduct.text = "\(productDetail.new_price!)rs"
        cell.labelOldPrice.text = "\(productDetail.price!)rs"

        let oldPrice = Double(productDetail.price!)
        let newPrice = Double(productDetail.new_price!)
        
        let percentage = (oldPrice! - newPrice) / oldPrice! * 100
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 1
        formatter.roundingMode = .up

        let strPercentage = String(describing: formatter.string(from: NSNumber(value: percentage))!)
        
        cell.labelOffLabel.text = "\(strPercentage)%off"
        
        cell.imageviewProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imageviewProduct.sd_setImage(with: URL.init(string: productDetail.product_image!)) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell.imageviewProduct.image = UIImage(named: "imageload")
            } else {
                // Successful in loading image
                cell.imageviewProduct.image = image
            }
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/2, height: 215)
        
    }
    
}
