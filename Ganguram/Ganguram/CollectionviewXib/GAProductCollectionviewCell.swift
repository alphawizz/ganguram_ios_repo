//
//  GAProductCollectionviewCell.swift
//  Ganguram
//
//  Created by Apple on 11/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import KRProgressHUD

class GAProductCollectionviewCell: UICollectionViewCell, FloatRatingViewDelegate {

    @IBOutlet weak var imageviewProduct: UIImageView!
    @IBOutlet weak var labelProductName: UILabel!
    @IBOutlet weak var viewRating: FloatRatingView!
    @IBOutlet weak var buttonWish: UIButton!
    @IBOutlet weak var buttonAddCard: UIButton!
    
    @IBOutlet weak var labelProductPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setRatingViewProperties()
        imageviewProduct.layer.cornerRadius = 3
        // Initialization code
    }
    
    func setRatingViewProperties()  {
           
           viewRating.delegate = self
           viewRating.contentMode = UIView.ContentMode.scaleAspectFit
           viewRating.type = .halfRatings
           viewRating.maxRating =  5
           viewRating.minRating =  0
       }
}

//----------------------------------------------------------------------------------------------------------------------------
//Mark -> From DashBoard Screen

extension GADashBoardViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return GAProduct.shared.arrayProductDeatil.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionviewList.dequeueReusableCell(withReuseIdentifier: String(describing: GAProductCollectionviewCell.self), for: indexPath) as! GAProductCollectionviewCell
        
        let productDetail = GAProduct.shared.arrayProductDeatil[indexPath.row]
        
        cell.labelProductName.text = productDetail.productName
        
        cell.imageviewProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imageviewProduct.sd_setImage(with: URL.init(string: productDetail.productImage!)) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell.imageviewProduct.image = UIImage(named: "imageload")
            } else {
                // Successful in loading image
                cell.imageviewProduct.image = image
            }
        }
        
        cell.labelProductPrice.text = "\(productDetail.price!) rs"
        cell.viewRating.rating = Double(productDetail.rating!)
        cell.buttonWish.tag = indexPath.row
        cell.buttonAddCard.tag = indexPath.row

        if productDetail.wishlist_status! {
            cell.buttonWish.setImage(UIImage.init(named: "wishlistRed"), for: .normal)

        } else {
            cell.buttonWish.setImage(UIImage.init(named: "blackheart"), for: .normal)
        }
        
        let alreadyExistInCard = arrayAddCardList.filter({$0.productId == productDetail.productId})
        
           if alreadyExistInCard.count == 0 {
            cell.buttonAddCard.setBackgroundImage(UIImage.init(named: "cart"), for: .normal)

           } else {
            cell.buttonAddCard.setBackgroundImage(UIImage.init(named: "adddedIncard"), for: .normal)
        }
        
        cell.buttonWish.addTarget(self, action: #selector(self.onWish), for: .touchUpInside)
        cell.buttonAddCard.addTarget(self, action: #selector(self.onAddCard), for: .touchUpInside)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/2, height: 245)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: String.init(describing: GAProductDetailViewController.self), sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "GAProductDetailViewController" {
            
            let vc = segue.destination as! GAProductDetailViewController
            vc.dictProdustDetail = GAProduct.shared.arrayProductDeatil[sender as! Int]
            let dictProduct = GAProduct.shared.arrayProductDeatil[sender as! Int]
            let alreadyExistInCard = arrayAddCardList.filter({$0.productId == dictProduct.productId})
            
               if alreadyExistInCard.count == 0 {
                vc.isAlreadyAddedInCart = false
               } else {
                vc.isAlreadyAddedInCart = true
            }
        }
    }
    
    @objc func onWish(buttonWish : UIButton)  {
        
        let productData = GAProduct.shared.arrayProductDeatil[buttonWish.tag]
        
        if productData.wishlist_status! {
            
            callApiForDeleteProductFromWishlist(productData : productData)
        } else {
            
            callAddWishApi(productData: productData)
        }
    }
    
    func callAddWishApi(productData : ProductDetail)  {
        
        if Connectivity.isConnectedToInternet() {
            
            //KRProgressHUD.show()
            
            let dictData : [String : Any] = ["product_id" : productData.productId!,
                                             "user_id" : GAUser.shared.loginInfoObj.userId!]
            
            GAService.shared.apiForAddWishList(param: dictData) { (isSucess, message, response) in
                
                //KRProgressHUD.dismiss()
                
                if isSucess! {
                   
                    let index = GAProduct.shared.arrayProductDeatil.firstIndex(where: {($0.productId) == productData.productId!})
                    
                    productData.wishlist_status = true
                    GAProduct.shared.arrayProductDeatil.remove(at: index!)
                    GAProduct.shared.arrayProductDeatil.insert(productData, at: index!)
                    self.view.makeToast("Product added in wishlist")
                    self.collectionviewList.reloadData()
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
    
    func callApiForDeleteProductFromWishlist(productData : ProductDetail)  {
        
        if Connectivity.isConnectedToInternet() {
            
            //KRProgressHUD.show()
            
            let dictData : [String : Any] = ["product_id" : productData.productId!,
                                             "user_id" : GAUser.shared.loginInfoObj.userId!]
            
            GAService.shared.apiForRemoveWishList(param: dictData) { (isSucess, message, response) in
                
                //KRProgressHUD.dismiss()
                
                if isSucess! {
                    
                    let index = GAProduct.shared.arrayProductDeatil.firstIndex(where: {($0.productId) == productData.productId!})
                    
                    productData.wishlist_status = false
                    GAProduct.shared.arrayProductDeatil.remove(at: index!)
                    GAProduct.shared.arrayProductDeatil.insert(productData, at: index!)
                    self.view.makeToast("Product removed from wishlist")
                    self.collectionviewList.reloadData()
                    
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
    @objc func onAddCard(buttonCard : UIButton)  {
        
        let productData = GAProduct.shared.arrayProductDeatil[buttonCard.tag]        
    
               if let objects = UserDefaults.standard.value(forKey: GAUserKey.AddToCard) as? Data {
                  let decoder = JSONDecoder()
                  if let cardList = try? decoder.decode(Array.self, from: objects) as [ProductDetail] {
                    
                    
                    // check Product already exist or not

                    let alreadyExistInCard = cardList.filter({$0.productId == productData.productId})
                                 
                                    if alreadyExistInCard.count == 0 {
                                        arrayAddCardList.removeAll()
                                        arrayAddCardList = cardList
                                        productData.dateAdded = Date()
                                        
                                        arrayAddCardList.append(productData)
                                        collectionviewList.reloadData()

                                       let encoder = JSONEncoder()
                                       if let encoded = try? encoder.encode(arrayAddCardList){
                                          UserDefaults.standard.set(encoded, forKey: GAUserKey.AddToCard)
                                        self.view.makeToast("Product added in card")
                                        labelAddToCardCount.text = "\(arrayAddCardList.count)"

                                       }
                           }
                  }
               } else {
                
                // Add Product first time
                productData.dateAdded = Date()
                arrayAddCardList.append(productData)
                
                let encoder = JSONEncoder()
                if let encoded = try? encoder.encode(arrayAddCardList){
                   UserDefaults.standard.set(encoded, forKey: GAUserKey.AddToCard)
                    self.view.makeToast("Product added in card")
                    collectionviewList.reloadData()
                    labelAddToCardCount.text = "\(arrayAddCardList.count)"
                }
        }
    }
}



//----------------------------------------------------------------------------------------------------------------------------
// Mark -> Form Category Product screen

extension GACategoryProductsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return GAProduct.shared.arrayProductDeatil.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionviewCategoryProducts.dequeueReusableCell(withReuseIdentifier: String(describing: GAProductCollectionviewCell.self), for: indexPath) as! GAProductCollectionviewCell
        
        let productDetail = GAProduct.shared.arrayProductDeatil[indexPath.row]
        
        cell.labelProductName.text = productDetail.productName
        
        cell.imageviewProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imageviewProduct.sd_setImage(with: URL.init(string: productDetail.productImage!)) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell.imageviewProduct.image = UIImage(named: "imageload")
            } else {
                // Successful in loading image
                cell.imageviewProduct.image = image
            }
        }
        
        cell.labelProductPrice.text = "\(productDetail.price!) rs"
        cell.viewRating.rating = Double(productDetail.rating!)
        
        if productDetail.wishlist_status! {
            cell.buttonWish.setImage(UIImage.init(named: "wishlistRed"), for: .normal)

        } else {
            cell.buttonWish.setImage(UIImage.init(named: "blackheart"), for: .normal)
        }
        
        let alreadyExistInCard = arrayAddCardList.filter({$0.productId == productDetail.productId})
        
           if alreadyExistInCard.count == 0 {
            cell.buttonAddCard.setBackgroundImage(UIImage.init(named: "cart"), for: .normal)

           } else {
            cell.buttonAddCard.setBackgroundImage(UIImage.init(named: "adddedIncard"), for: .normal)
        }
        
        cell.buttonWish.tag = indexPath.row
        cell.buttonAddCard.tag = indexPath.row
        
        cell.buttonWish.addTarget(self, action: #selector(self.onWish), for: .touchUpInside)
        cell.buttonAddCard.addTarget(self, action: #selector(self.onAddCard), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/2, height: 245)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: String.init(describing: GAProductDetailViewController.self), sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "GAProductDetailViewController" {
            
            let vc = segue.destination as! GAProductDetailViewController
            vc.dictProdustDetail = GAProduct.shared.arrayProductDeatil[sender as! Int]
            
              let dictProduct = GAProduct.shared.arrayProductDeatil[sender as! Int]
                      let alreadyExistInCard = arrayAddCardList.filter({$0.productId == dictProduct.productId})
                      
                         if alreadyExistInCard.count == 0 {
                          vc.isAlreadyAddedInCart = false
                         } else {
                          vc.isAlreadyAddedInCart = true
                      }
        }
    }
    
    @objc func onAddCard(buttonCard : UIButton)  {
           
           let productData = GAProduct.shared.arrayProductDeatil[buttonCard.tag]
       
                  if let objects = UserDefaults.standard.value(forKey: GAUserKey.AddToCard) as? Data {
                     let decoder = JSONDecoder()
                     if let cardList = try? decoder.decode(Array.self, from: objects) as [ProductDetail] {
                       
                       
                       // check Product already exist or not

                       let alreadyExistInCard = cardList.filter({$0.productId == productData.productId})
                                    
                                       if alreadyExistInCard.count == 0 {
                                           arrayAddCardList.removeAll()
                                           arrayAddCardList = cardList
                                        productData.dateAdded = Date()
                                        
                                           arrayAddCardList.append(productData)
                                        let index = GAProduct.shared.arrayProductDeatil.firstIndex(where: {($0.productId) == productData.productId!})
                                                                            
                                                                               GAProduct.shared.arrayProductDeatil.remove(at: index!)
                                                                               GAProduct.shared.arrayProductDeatil.insert(productData, at: index!)
                                        
                                           collectionviewCategoryProducts.reloadData()

                                          let encoder = JSONEncoder()
                                          if let encoded = try? encoder.encode(arrayAddCardList){
                                             UserDefaults.standard.set(encoded, forKey: GAUserKey.AddToCard)
                                           self.view.makeToast("Product added in card")

                                          }
                              }
                     }
                  } else {
                   
                   // Add Product first time
                       productData.dateAdded = Date()
                   
                   arrayAddCardList.append(productData)
                   
                   let encoder = JSONEncoder()
                   if let encoded = try? encoder.encode(arrayAddCardList){
                      UserDefaults.standard.set(encoded, forKey: GAUserKey.AddToCard)
                       self.view.makeToast("Product added in card")
                  
                    
                    let index = GAProduct.shared.arrayProductDeatil.firstIndex(where: {($0.productId) == productData.productId!})
                                                                                                  GAProduct.shared.arrayProductDeatil.remove(at: index!)
                                                                                                  GAProduct.shared.arrayProductDeatil.insert(productData, at: index!)
                       collectionviewCategoryProducts.reloadData()
                   }
           }
       }
    
    
    @objc func onWish(buttonWish : UIButton)  {
           
           let productData = GAProduct.shared.arrayProductDeatil[buttonWish.tag]
           
           if productData.wishlist_status! {
               
               callApiForDeleteProductFromWishlist(productData : productData)
           } else {
               
               callAddWishApi(productData: productData)
           }
       }
       
       func callAddWishApi(productData : ProductDetail)  {
           
           if Connectivity.isConnectedToInternet() {
               
               //KRProgressHUD.show()
               
               let dictData : [String : Any] = ["product_id" : productData.productId!,
                                                "user_id" : GAUser.shared.loginInfoObj.userId!]
               
               GAService.shared.apiForAddWishList(param: dictData) { (isSucess, message, response) in
                   
                   //KRProgressHUD.dismiss()
                   
                   if isSucess! {
                      
                       let index = GAProduct.shared.arrayProductDeatil.firstIndex(where: {($0.productId) == productData.productId!})
                       
                       productData.wishlist_status = true
                       GAProduct.shared.arrayProductDeatil.remove(at: index!)
                       GAProduct.shared.arrayProductDeatil.insert(productData, at: index!)
                       self.view.makeToast("Product added in wishlist")
                       self.collectionviewCategoryProducts.reloadData()
                   } else {
                       self.view.makeToast(message)
                   }
               }
           } else {
               self.view.makeToast(GAMessage.networkError)
           }
       }
       
       
       func callApiForDeleteProductFromWishlist(productData : ProductDetail)  {
           
           if Connectivity.isConnectedToInternet() {
               
               //KRProgressHUD.show()
               
               let dictData : [String : Any] = ["product_id" : productData.productId!,
                                                "user_id" : GAUser.shared.loginInfoObj.userId!]
               
               GAService.shared.apiForRemoveWishList(param: dictData) { (isSucess, message, response) in
                   
                   //KRProgressHUD.dismiss()
                   
                   if isSucess! {
                       
                       let index = GAProduct.shared.arrayProductDeatil.firstIndex(where: {($0.productId) == productData.productId!})
                       
                       productData.wishlist_status = false
                       GAProduct.shared.arrayProductDeatil.remove(at: index!)
                       GAProduct.shared.arrayProductDeatil.insert(productData, at: index!)
                       self.view.makeToast("Product removed from wishlist")
                       self.collectionviewCategoryProducts.reloadData()
                       
                   } else {
                       self.view.makeToast(message)
                   }
               }
           } else {
               self.view.makeToast(GAMessage.networkError)
           }
       }
}
