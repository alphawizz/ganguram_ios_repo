//
//  GAAboutUsViewController.swift
//  Ganguram
//
//  Created by Apple on 11/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class GAAboutUsViewController: UIViewController {

    @IBOutlet weak var labelAboutUs: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.awakeFromNib()

        labelAboutUs.text = "A license (American English) or licence (British English)[1] is an official permission or permit to do, use, or own something (as well as the document of that permission or permit).[1] A license can be granted by a party to another party as an element of an agreement between those parties. A shorthand definition of a license is In particular, a license may be issued by authorities, to allow an activity that would otherwise be forbidden. It may require paying a fee or proving a capability. The requirement may also serve to keep the authorities informed on a type of activity, and to give them the opportunity to set conditions and limitations.A licensor may grant a license under intellectual property laws to authorize a use (such as copying software or using a patented invention) to a licensee, sparing the licensee from a claim of infringement brought by the licensor.[2] A license under intellectual property commonly has several components beyond the grant itself, including a term, territory, renewal provisions, and other limitations deemed vital to the licensor.Term: many licenses are valid for a particular length of time. This protects the licensor should the value of the license increase, or market conditions change. It also preserves enforceability by ensuring that no license extends beyond the term of the agreement.Territory: a license may stipulate what territory the rights pertain to. For example, a license with a territory limited to"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.awakeFromNib()
    }
}
