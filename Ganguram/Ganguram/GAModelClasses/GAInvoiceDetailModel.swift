//
//  GAInvoiceDetailModel.swift
//  Ganguram
//
//  Created by Apple on 21/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class GAInvoice {
    
    var arrayInvoiceList = [InvoiceDetail]()
    static let shared = GAInvoice()
    
    func saveOfferData(arrayProducts : [[String : Any]]) {
        
        arrayInvoiceList.removeAll()
        
        for  dict in arrayProducts {
            
            let deatil = InvoiceDetail.init(json: dict)
            arrayInvoiceList.append(deatil)
        }
    }
}


class InvoiceDetail {
    
    var invoice_Id: String?
    var totalPrice: String?
    var product_id: String?
    var price: String?
    var product_name: String?
    var productQuantity: String?
    var delivery_address: String?
    var sale_item_id :  String?
    
    
    init(json: [String: Any]) {
        
        self.invoice_Id = json["sale_id"] as? String ?? ""
        self.totalPrice = json["total_amount"] as? String ?? ""
        self.product_id = json["product_id"] as? String ?? ""
        self.price = json["price"] as? String ?? ""
        self.product_name = json["product_name"] as? String ?? ""
        self.productQuantity = json["qty"] as? String ?? ""
        self.delivery_address = json["delivery_address"] as? String ?? ""
        self.sale_item_id = json["sale_item_id"] as? String ?? ""
        
        
    }
}







//"sale_item_id": "1176",
//          "sale_id": "760",
//          "product_id": "95",
//          "product_name": "abar-khabo",
//          "qty": "4",
//          "unit": "abar khabo",
//          "unit_value": "2",
//          "price": "233",
//          "qty_in_kg": "0",
//          "rewards": "00",
//          "user_id": "228",
//          "on_date": "2019-12-30",
//          "delivery_time_from": "10:35:04am",
//          "delivery_time_to": "",
//          "status": "0",
//          "note": "",
//          "is_paid": "0",
//          "shipping_charge": "20",
//          "coupon_discount": "",
//          "total_amount": "952",
//          "total_rewards": "",
//          "total_kg": "0",
//          "total_items": "0",
//          "socity_id": "0",
//          "delivery_address": "inodre indore mp",
//          "location_id": "0",
//          "delivery_charge": "0",
//          "new_store_id": "0",
//          "assign_to": "0",
//          "payment_method": "",
//          "First_name": "rahul",
//          "Last_name": "rahul@gmail.com",
//          "email": "rahul@gmail.com",
//          "zip_code": "452154",
//          "product_description": "thfghfgdgh",
//          "product_image": "abar-khabo.jpg",
//          "category_id": "18",
//          "product_pincode": "453551",
//          "in_stock": "1",
//          "mrp": "0",
//          "tax": "1",
//          "increament": "0",
//          "loca_category": "0",
//          "quantity": "0",
//          "net_weight": "0",
//          "State_code": "18",
//          "Location_filter": "0",
//          "thumbnails_image": "plain-shankh-sandesh6.jpg,plain-shankh-sandesh6.jpg,plain-ratabi-sandesh6.jpg"
