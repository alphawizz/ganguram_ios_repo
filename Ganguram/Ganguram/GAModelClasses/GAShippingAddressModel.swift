//
//  GAShippingAddressModel.swift
//  Ganguram
//
//  Created by Apple on 17/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class GAShippingAddressList {
    
    var arrayShippingAddress = [ShippingAddress]()
    static let shared = GAShippingAddressList()

    func saveShippingAddessData(arrayProducts : [[String : Any]]) {
        
        arrayShippingAddress.removeAll()

        for  dict in arrayProducts {

            let deatil = ShippingAddress.init(json: dict)
            arrayShippingAddress.append(deatil)
        }
    }
}

class ShippingAddress : Encodable, Decodable {

var Address1: String?
var Address2: String?
    var City: String?
    var Pincode: String?
    var State: String?
    var User_id: String?
    var id: String?
    
    init(json: [String: Any]) {
           
         self.Address1 = json["Address1"] as? String ?? ""
         self.Address2 = json["Address2"] as? String ?? ""
         self.City = json["City"] as? String ?? ""
         self.Pincode = json["Pincode"] as? String ?? ""
         self.State = json["State"] as? String ?? ""
         self.User_id = json["User_id"] as? String ?? ""
         self.id = json["id"] as? String ?? ""
        
}
}
