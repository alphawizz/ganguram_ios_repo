//
//  GACategoryModel.swift
//  Ganguram
//
//  Created by Apple on 15/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class GACategory {
    
    var arrayCategoryDeatil = [CategoryDetail]()
    static let shared = GACategory()

    func saveCategoryData(arrayProducts : [[String : Any]]) {
        
        arrayCategoryDeatil.removeAll()

        for  dict in arrayProducts {

            let deatil = CategoryDetail.init(json: dict)
            arrayCategoryDeatil.append(deatil)
        }
    }
}

class CategoryDetail {
    
    var description: String?
    var id: String?
    var image: String?
    var leval: String?
    var parent: String?
    var slug: String?
    var status: String?
    var title: String?
    
    init(json: [String: Any]) {
        
        self.description = json["description"] as? String ?? ""
        self.id = json["id"] as? String ?? ""
        if let productImage = json["image"] as? String {
            self.image = GABaseUrlProductImages + productImage
        }
        self.leval = json["leval"] as? String ?? ""
        self.parent = json["parent"] as? String ?? ""
        self.slug = json["slug"] as? String ?? ""
        self.status = json["status"] as? String ?? ""
        self.title = json["title"] as? String ?? ""
    }
}
