//
//  GANotificationModel.swift
//  Ganguram
//
//  Created by Apple on 30/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class GANotification {
    
    var arrayNotificationList = [NotificationData]()
    static let shared = GANotification()

    func saveNotificationData(arrayProducts : [[String : Any]]) {
        
        arrayNotificationList.removeAll()

        for  dict in arrayProducts {

            let deatil = NotificationData.init(json: dict)
            arrayNotificationList.append(deatil)
        }
    }
}


class NotificationData {
    
    var id: String?
    var user_id: String?
    var notification: String?
    var invoice_id: String?
    var status: String?
    var device_id: String?
    var date : String?
    var time : String?
    var watched : String?
    
    init(json: [String: Any]) {
        
        self.id = json["id"] as? String ?? ""
        self.user_id = json["user_id"] as? String ?? ""
        self.notification = json["notification"] as? String ?? ""
        self.invoice_id = json["invoice_id"] as? String ?? ""
        self.status = json["status"] as? String ?? ""
        self.device_id = json["device_id"] as? String ?? ""
        self.date = json["date"] as? String ?? ""
        self.time = json["time"] as? String ?? ""
        self.watched = json["watched"] as? String ?? ""
    }
}
