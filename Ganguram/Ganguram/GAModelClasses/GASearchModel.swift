//
//  GASearchModel.swift
//  Ganguram
//
//  Created by Apple on 16/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


class GASearchedProduct {
    
    var arraySearchDeatil = [SearchProductDetail]()
    static let shared = GASearchedProduct()

    func saveSearchedData(arrayProducts : [[String : Any]]) {
        
        arraySearchDeatil.removeAll()

        for  dict in arrayProducts {

            let deatil = SearchProductDetail.init(json: dict)
            arraySearchDeatil.append(deatil)
        }
    }
}

class SearchProductDetail {
    
    var product_id: String?
    var product_name: String?
    var product_description: String?
    var product_image : String?
    var category_id: String?
    var product_pincode: String?
    var in_stock: String?
    var price: String?
    var mrp: String?
    var tax: String?
    var unit_value: String?
    var unit: String?
    var increament: String?
    var rewards: String?
    var loca_category: String?
    var net_weight: String?
    var State_code: Int?
    var Location_filter: Int?
    var thumbnails_image: String?
    var comment: String?
    var rating: Int?
    var new_price: Int?
   

    init(json: [String: Any]) {
        
        self.product_id = json["product_id"] as? String ?? ""
        self.product_name = json["product_name"] as? String ?? ""
        self.product_description = json["product_description"] as? String ?? ""
       
        self.category_id = json["category_id"] as? String ?? ""
        self.product_pincode = json["product_pincode"] as? String ?? ""
        self.in_stock = json["in_stock"] as? String ?? ""
        
        self.price = json["price"] as? String ?? ""
        self.mrp = json["mrp"] as? String ?? ""
        self.tax = json["tax"] as? String ?? ""
        self.unit_value = json["unit_value"] as? String ?? ""
        self.increament = json["increament"] as? String ?? ""
        self.rewards = json["rewards"] as? String ?? ""
        
        if let productImage = json["product_image"] as? String {
            self.product_image = GABaseUrlProductImages + productImage
        }
        
        self.loca_category = json["loca_category"] as? String ?? ""
        self.net_weight = json["net_weight"] as? String ?? ""
        self.State_code = json["State_code"] as? Int ?? 0
        self.Location_filter = json["Location_filter"] as? Int ?? 0
        self.rewards = json["rewards"] as? String ?? ""
        self.tax = json["tax"] as? String ?? ""
        self.thumbnails_image = json["thumbnails_image"] as? String ?? ""
        self.unit = json["unit"] as? String ?? ""
        self.rating = json["rating"] as? Int ?? 0
        self.new_price = json["new_price"] as? Int ?? 0
}
}
