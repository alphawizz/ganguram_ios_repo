//
//  GAInvoiceProductsModel.swift
//  Ganguram
//
//  Created by Apple on 21/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class GAInvoiceProducts {
    
    var arrayInvoiceProductList = [InvoiceProductDetail]()
    static let shared = GAInvoiceProducts()

    func saveInvoiceProductData(arrayProducts : [[String : Any]]) {
        
        arrayInvoiceProductList.removeAll()

        for  dict in arrayProducts {

            let deatil = InvoiceProductDetail.init(json: dict)
            arrayInvoiceProductList.append(deatil)
        }
    }
}


class InvoiceProductDetail {

var invoice_Id: String?
var totalPrice: String?
var product_id: String?
var price: String?
var product_name: String?
var productQuantity: String?
    var productImage : String?
    
    init(json: [String: Any]) {
    
    self.invoice_Id = json["sale_id"] as? String ?? ""
    self.totalPrice = json["total_amount"] as? String ?? ""
    self.product_id = json["product_id"] as? String ?? ""
    self.price = json["price"] as? String ?? ""
    self.product_name = json["product_name"] as? String ?? ""
    self.productQuantity = json["qty"] as? String ?? ""

        if let productImage = json["product_image"] as? String {
            self.productImage = GABaseUrlProductImages + productImage
        }
    }
}
