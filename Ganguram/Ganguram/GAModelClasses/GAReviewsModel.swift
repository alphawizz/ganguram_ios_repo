//
//  GAReviewsModel.swift
//  Ganguram
//
//  Created by Apple on 16/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class GAReviewList {
    
    var arrayReviewData = [ReviewDetail]()
    static let shared = GAReviewList()

    func saveReviewData(arrayProducts : [[String : Any]]) {
        
        arrayReviewData.removeAll()

        for  dict in arrayProducts {

            let deatil = ReviewDetail.init(json: dict)
            arrayReviewData.append(deatil)
        }
    }
}




class ReviewDetail {

var id: String?
var product_id: String?
    var user_id: String?
    var rating: Int?
    var comment: String?
    var approved: String?
    var time: String?
    var user_fullname: String?
    
    init(json: [String: Any]) {
           
         self.id = json["id"] as? String ?? ""
         self.product_id = json["product_id"] as? String ?? ""
         self.user_id = json["user_id"] as? String ?? ""
         self.rating = json["rating"] as? Int ?? 0
         self.comment = json["comment"] as? String ?? ""
         self.approved = json["approved"] as? String ?? ""
         self.time = json["time"] as? String ?? ""
         self.user_fullname = json["user_fullname"] as? String ?? ""
        
}
}
