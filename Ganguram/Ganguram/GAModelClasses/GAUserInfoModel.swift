//
//  GAUserInfoModel.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class GAUser {
    
    var loginInfoObj: loginData!
    static let shared = GAUser()
   
     func getUserInfo() {
        
        if let decoded  = UserDefaults.standard.object(forKey: GAUserKey.LoginData) as? Data {
            
            do {
                let dictUserDetail = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as! [String: Any]
                self.loginInfoObj = loginData.init(json: dictUserDetail["data"] as! [String : Any])
                
            } catch {
                print(error.localizedDescription)
            }
            }
        }
}


class loginData: Codable {
    
    var created: String?
    var firstName: String?
    var houseNo: String?
    var lastName: String?
    var mobileVerified: Bool?
    var modified: String?
    var pincode: String?
    var regCode: String?
    var rewards: String?
    var socityId: String?
    var status: String?
    var userBdate: String?
    var userCity: String?
    var userEmail: String?
    var userFullname: String?
    var userGcmCode: String?
    var userId: String?
    var userImage: String?
    var userIosToken: String?
    var userPassword: String?
    var userPhone: String?
    var varificationCode: String?
    var varifiedToken: String?
    var wallet: String?

    init(json: [String: Any]) {
        
        self.created = json["created"] as? String ?? ""
        self.firstName = json["firstname"] as? String ?? ""
        self.houseNo = json["house_no"] as? String ?? ""
        self.lastName = json["lastname"] as? String ?? ""
        self.mobileVerified = json["mobile_verified"] as? Bool ?? false
        self.modified = json["modified"] as? String ?? ""
        self.pincode = json["pincode"] as? String ?? ""
        self.regCode = json["reg_code"] as? String ?? ""
        self.rewards = json["rewards"] as? String ?? ""
        self.socityId = json["socity_id"] as? String ?? ""
        self.status = json["status"] as? String ?? ""
        self.userBdate = json["user_bdate"] as? String ?? ""
        self.userCity = json["user_city"] as? String ?? ""
        self.userEmail = json["user_email"] as? String ?? ""
        self.userFullname = json["user_fullname"] as? String ?? ""
        self.userGcmCode = json["user_gcm_code"] as? String ?? ""
        self.userId = json["user_id"] as? String ?? ""
       
        if let userImage = json["user_image"] as? String {
            self.userImage =  GABaseUrlUserImages + userImage
        }
        
        self.userIosToken = json["user_ios_token"] as? String ?? ""
        self.userPassword = json["user_password"] as? String ?? ""
        self.userPhone = json["user_phone"] as? String ?? ""
        self.varificationCode = json["varification_code"] as? String ?? ""
        self.varifiedToken = json["varified_token"] as? String ?? ""
        self.wallet = json["wallet"] as? String ?? ""
}
    
    
}
