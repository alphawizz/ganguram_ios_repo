//
//  GAProductModel.swift
//  Ganguram
//
//  Created by Apple on 15/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class GAProduct  {
    
    var arrayProductDeatil = [ProductDetail]()
    static let shared = GAProduct()
  

    func saveProductDetailData(arrayProducts : [[String : Any]]) {
        
        arrayProductDeatil.removeAll()

        for  dict in arrayProducts {

            let deatil = ProductDetail.init(json: dict)
            arrayProductDeatil.append(deatil)
        }
    }
}

class ProductDetail  : NSObject, NSCoding, Encodable, Decodable {
    
    var LocationFilter: String?
    var StateCode: String?
    var categoryId: String?
    var images : [String]?
    var comment: String?
    var inStock: String?
    var increament: String?
    var locaCategory: String?
    var mrp: String?
    var net_weight: String?
    var price: String?
    var productDescription: String?
    var productId: String?
    var productImage: String?
    var productName: String?
    var productPincode: String?
    var quantity: Int?
    var rating: Int?
    var rewards: String?
    var tax: String?
    var thumbnailsImage: String?
    var unit: String?
    var unitValue: String?
    var wishlist_status: Bool?
    var productCount : Int?
    var dateAdded : Date?
    

    init(json: [String: Any]) {
        
        self.LocationFilter = json["Location_filter"] as? String ?? ""
        self.StateCode = json["State_code"] as? String ?? ""
        self.categoryId = json["category_id"] as? String ?? ""
        self.comment = json["comment"] as? String ?? ""
        self.images = json["images"] as? [String] ?? []
        self.inStock = json["in_stock"] as? String ?? ""
        self.increament = json["increament"] as? String ?? ""
        self.locaCategory = json["loca_category"] as? String ?? ""
        self.mrp = json["mrp"] as? String ?? ""
        self.net_weight = json["net_weight"] as? String ?? ""
        self.price = json["price"] as? String ?? ""
        self.productDescription = json["product_description"] as? String ?? ""
        self.productId = json["product_id"] as? String ?? ""
        
        if let productImage = json["product_image"] as? String {
            self.productImage = GABaseUrlProductImages + productImage
        }
        
        self.productName = json["product_name"] as? String ?? ""
        self.productPincode = json["product_pincode"] as? String ?? ""
        self.quantity = json["quantity"] as? Int ?? 0
        self.rating = json["rating"] as? Int ?? 0
        self.rewards = json["rewards"] as? String ?? ""
        self.tax = json["tax"] as? String ?? ""
        self.thumbnailsImage = json["thumbnails_image"] as? String ?? ""
        self.unit = json["unit"] as? String ?? ""
        self.unitValue = json["unit_value"] as? String ?? ""
        self.wishlist_status = json["wishlist_status"] as? Bool ?? false
        self.productCount = 1
        
        if let objects = UserDefaults.standard.value(forKey: GAUserKey.AddToCard) as? Data {
            let decoder = JSONDecoder()
            if let cardList = try? decoder.decode(Array.self, from: objects) as [ProductDetail] {

                if let index = cardList.firstIndex(where: {($0.productId) == json["product_id"] as? String ?? ""}) {
                    self.dateAdded = cardList[index].dateAdded
                }
                
                }
        }
        }

    
    func encode(with coder: NSCoder) {
        coder.encode(self.LocationFilter, forKey: "Location_filter")

    }
    
    required init?(coder: NSCoder) {
        self.LocationFilter = coder.decodeObject(forKey: "Location_filter") as? String ?? ""

    }
}
  

