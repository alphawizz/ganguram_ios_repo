//
//  GAServiceManager.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


class GAService: NSObject {
    
    static var shared: GAService = GAService()
    
    //MARK:- Login Service
    func apiForUserLogin( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.LoginApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        do {
                            
                            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: true)
                            
                            GADefault.setValue(encodedData, forKey: GAUserKey.LoginData)
                            GADefault.synchronize()
                            GAUser.shared.getUserInfo()
                            
                            print(encodedData)
                        } catch {
                            print(error.localizedDescription)
                            // or display a dialog
                        }
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    
    //MARK:- Signup Service
    
    func apiForUserSignup( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.RegistrationApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        do {
                            
                            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: true)
                            
                            GADefault.setValue(encodedData, forKey: GAUserKey.LoginData)
                            GADefault.synchronize()
                            GAUser.shared.getUserInfo()
                            
                            print(encodedData)
                        } catch {
                            print(error.localizedDescription)
                            // or display a dialog
                        }
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- Generate OTP
    
    func apiForGenerateOTP( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.generateOtpAPi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        do {
                            
                            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: true)
                            
                            //  GADefault.setValue(encodedData, forKey: GAUserKey.LoginData)
                            //GADefault.synchronize()
                            // GAUser.shared.getUserInfo()
                            
                            print(encodedData)
                        } catch {
                            print(error.localizedDescription)
                            // or display a dialog
                        }
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- Verify OTP
    
    func apiForVerifyOTP( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.otpVarificationAPi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        do {
                            
                            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: true)
                            
                            GADefault.setValue(encodedData, forKey: GAUserKey.LoginData)
                            GADefault.synchronize()
                            GAUser.shared.getUserInfo()
                            
                            print(encodedData)
                        } catch {
                            print(error.localizedDescription)
                            // or display a dialog
                        }
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- Verify OTP
    
    func apiForSocialLogin( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.socialLoginApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        do {
                            
                            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: true)
                            
                            GADefault.setValue(encodedData, forKey: GAUserKey.LoginData)
                            GADefault.synchronize()
                            GAUser.shared.getUserInfo()
                            
                            print(encodedData)
                        } catch {
                            print(error.localizedDescription)
                            // or display a dialog
                        }
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    
    //MARK:- Add WishList
    
    func apiForAddWishList( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.addWishlistApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        do {
                            
                            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: true)
                            print(encodedData)
                        } catch {
                            print(error.localizedDescription)
                            // or display a dialog
                        }
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    
    
      //MARK:- Get WishList
      
      func apiForGetWishList( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
          
          GAApiManager.shared.POSTRequest(strURL: GAApi.getWishlistApi, Param: param) { (result) in
              
              switch(result) {
              case .success(let value):
                  
                  if let code = (value as! [String:Any])["status"] {
                      
                      let status = "\(code)"
                      
                      let response = value as! [String: Any]
                      
                      if status == APIStatus.success.rawValue    {
                          
                          do {
                              
                              let encodedData = try NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: true)
                              print(encodedData)
                          } catch {
                              print(error.localizedDescription)
                              // or display a dialog
                          }
                          
                          completionHandler(true,"", response)
                          
                      } else {
                          completionHandler(false,response["message"] as? String, response)
                      }
                  }
              case .error(let error):
                  completionHandler(false,error.localizedDescription, [:])
              }
          }
      }
    
    
    //MARK:-Remove WishList
         
         func apiForRemoveWishList( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
             
             GAApiManager.shared.POSTRequest(strURL: GAApi.removeWishlistApi, Param: param) { (result) in
                 
                 switch(result) {
                 case .success(let value):
                     
                     if let code = (value as! [String:Any])["status"] {
                         
                         let status = "\(code)"
                         
                         let response = value as! [String: Any]
                         
                         if status == APIStatus.success.rawValue    {
                             
                             do {
                                 
                                 let encodedData = try NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: true)
                                 print(encodedData)
                             } catch {
                                 print(error.localizedDescription)
                                 // or display a dialog
                             }
                             
                             completionHandler(true,"", response)
                             
                         } else {
                             completionHandler(false,response["message"] as? String, response)
                         }
                     }
                 case .error(let error):
                     completionHandler(false,error.localizedDescription, [:])
                 }
             }
         }
    
    //MARK:- Get ProductList By locationID
    
    func apiForGetProductListOfSelectedLocation( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.getAllProductApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        do {
                            
                            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: true)
                            print(encodedData)
                        } catch {
                            print(error.localizedDescription)
                            // or display a dialog
                        }
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    func apiForGetAllCategoryList(completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.GETRequest(strURL: GAApi.getAllCategory) { (result)
            in
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        do {
                            
                            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: true)
                            print(encodedData)
                        } catch {
                            print(error.localizedDescription)
                            // or display a dialog
                        }
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- Get ProductList By Category
       
       func apiForGetProductByCategory( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
           
           GAApiManager.shared.POSTRequest(strURL: GAApi.getAllProductByCatgeory, Param: param) { (result) in
               
               switch(result) {
               case .success(let value):
                   
                   if let code = (value as! [String:Any])["status"] {
                       
                       let status = "\(code)"
                       
                       let response = value as! [String: Any]
                       
                       if status == APIStatus.success.rawValue    {
                           
                           completionHandler(true,"", response)
                           
                       } else {
                           completionHandler(false,response["message"] as? String, response)
                       }
                   }
               case .error(let error):
                   completionHandler(false,error.localizedDescription, [:])
               }
           }
       }
    
    //MARK:- Get offer on product

    func apiForGetOffers(completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.GETRequest(strURL: GAApi.getOffers) { (result)
            in
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- Get Search product
          
          func apiForGetSearchedProduct( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
              
              GAApiManager.shared.POSTRequest(strURL: GAApi.getSearchedProductApi, Param: param) { (result) in
                  
                  switch(result) {
                  case .success(let value):
                      
                      if let code = (value as! [String:Any])["status"] {
                          
                          let status = "\(code)"
                          
                          let response = value as! [String: Any]
                          
                          if status == APIStatus.success.rawValue    {
                              
                              completionHandler(true,"", response)
                              
                          } else {
                              completionHandler(false,response["message"] as? String, response)
                          }
                      }
                  case .error(let error):
                      completionHandler(false,error.localizedDescription, [:])
                  }
              }
          }
    
    
    //MARK:- Apply Filter Api
    
    func apiForApplyFilterApi( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.applyFilterApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- Get Search product
    
    func apiForAddReview( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.addReviewApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- Get Review Api
    
    func apiForGetReviews( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.getReviewsApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    
    //MARK:- Add Address Api
       
       func apiForAddAddress( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
           
           GAApiManager.shared.POSTRequest(strURL: GAApi.AddAddressApi, Param: param) { (result) in
               
               switch(result) {
               case .success(let value):
                   
                   if let code = (value as! [String:Any])["status"] {
                       
                       let status = "\(code)"
                       
                       let response = value as! [String: Any]
                       
                       if status == APIStatus.success.rawValue    {
                           
                           completionHandler(true,"", response)
                           
                       } else {
                           completionHandler(false,response["message"] as? String, response)
                       }
                   }
               case .error(let error):
                   completionHandler(false,error.localizedDescription, [:])
               }
           }
       }
    
    //MARK:- Get Address Api
    
    func apiForGetAddress( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.GetAddressApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- delete Address Api
       
       func apiForDeleteAddress( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
           
           GAApiManager.shared.POSTRequest(strURL: GAApi.DeleteAddressApi, Param: param) { (result) in
               
               switch(result) {
               case .success(let value):
                   
                   if let code = (value as! [String:Any])["status"] {
                       
                       let status = "\(code)"
                       
                       let response = value as! [String: Any]
                       
                       if status == APIStatus.success.rawValue    {
                           
                           completionHandler(true,"", response)
                           
                       } else {
                           completionHandler(false,response["message"] as? String, response)
                       }
                   }
               case .error(let error):
                   completionHandler(false,error.localizedDescription, [:])
               }
           }
       }
    
    
    //MARK:- Update Address Api
    
    func apiForUpdateAddress( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.UpdateAddressApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- Add Contact us
       
       func apiForContactUs( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
           
           GAApiManager.shared.POSTRequest(strURL: GAApi.contactUsApi, Param: param) { (result) in
               
               switch(result) {
               case .success(let value):
                   
                   if let code = (value as! [String:Any])["status"] {
                       
                       let status = "\(code)"
                       
                       let response = value as! [String: Any]
                       
                       if status == APIStatus.success.rawValue    {
                           
                           completionHandler(true,"", response)
                           
                       } else {
                           completionHandler(false,response["message"] as? String, response)
                       }
                   }
               case .error(let error):
                   completionHandler(false,error.localizedDescription, [:])
               }
           }
       }
    
    //MARK:- Add Contact us
    
    func apiForResetPassword( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.ResetPasswordApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    
    //MARK:- Add Billing
    
    func apiForBiling( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.paymentBillingApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- Add Billing
    
    func apiForConfirmPayment( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.confirmedPaymentApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- Add Billing
    
    func apiForGetAllInvoice( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.getMyAllInvoiceApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    //MARK:- Add Billing
       
       func apiForInvoiceDetail( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
           
           GAApiManager.shared.POSTRequest(strURL: GAApi.getInvoiceDetailApi, Param: param) { (result) in
               
               switch(result) {
               case .success(let value):
                   
                   if let code = (value as! [String:Any])["status"] {
                       
                       let status = "\(code)"
                       
                       let response = value as! [String: Any]
                       
                       if status == APIStatus.success.rawValue    {
                           
                           completionHandler(true,"", response)
                           
                       } else {
                           completionHandler(false,response["message"] as? String, response)
                       }
                   }
               case .error(let error):
                   completionHandler(false,error.localizedDescription, [:])
               }
           }
       }
    
    
    //MARK:- Add UpdateProfile
    
    func apiForUpdateProfile( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.UpdateUserProfile, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
  
    
    //MARK:- Call api for Banner

    func apiForBanner(completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.GETRequest(strURL: GAApi.bannerApi) { (result)
            in
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
    
    func apiForNotificationList( param:[String:Any], completionHandler:@escaping (_ isSuccess:Bool?, _ strError:String?, _ response:[String: Any]?)->Void) {
        
        GAApiManager.shared.POSTRequest(strURL: GAApi.notificatilListApi, Param: param) { (result) in
            
            switch(result) {
            case .success(let value):
                
                if let code = (value as! [String:Any])["status"] {
                    
                    let status = "\(code)"
                    
                    let response = value as! [String: Any]
                    
                    if status == APIStatus.success.rawValue    {
                        
                        completionHandler(true,"", response)
                        
                    } else {
                        completionHandler(false,response["message"] as? String, response)
                    }
                }
            case .error(let error):
                completionHandler(false,error.localizedDescription, [:])
            }
        }
    }
    
}






