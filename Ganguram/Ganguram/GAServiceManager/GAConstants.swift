//
//  GAConstants.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

//Mark:- Constant Keys for value
struct GAUserKey {
    static let LoginData = "LoginData"
    static let AddToCard = "AddToCard"
    static let SavedAddress = "SavedAddress"
}


////Mark:- Save LoggedIn User info For Login Session -
struct UserProfileData {
    static let key = "userProfile"
    static func save(_ value: loginData!) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: key)
    }

    static func get() -> loginData! {
        var userData: loginData!
        if let data = UserDefaults.standard.value(forKey: key) as? Data {
            userData = try? PropertyListDecoder().decode(loginData.self, from: data)
            return userData!
        } else {
            return userData
        }
    }
    static func remove() {
        UserDefaults.standard.removeObject(forKey: key)
    }
}


//Mark:- Base Url
// IP Server : 204.11.58.189
let GABaseURL = "https://ganguram.com/sweet_api/"
let GABaseUrlProductImages = "https://ganguram.com/sweetadmin/uploads/products/"
let GABaseUrlUserImages = "https://ganguram.com/sweet_api/"
let GABaseUrlBanner = "https://ganguram.com/sweet_api/Banner/"



let GADefault  = UserDefaults()

//Mark:- AppSesvices
struct GAApi  {
    
    static let LoginApi = "Authentication/login"
    static let RegistrationApi = "Authentication/registration"
    static let generateOtpAPi = "Authentication/generateOtp"
    static let otpVarificationAPi = "Authentication/otpVarification"
    static let socialLoginApi = "SocialLogin/Login"
    static let addWishlistApi = "Products/addWishList"
    static let getWishlistApi = "Products/getWishList"
    static let removeWishlistApi = "Products/removeWishlist"
    static let homeProductApi = "Products/HomeProducts"
    static let stateApi = "Products/State"
    static let getAllProductApi = "Products/getAllProduct"
    static let getAllCategory = "Products/Category"
    static let getAllProductByCatgeory = "Products/ProductByCategory"
    static let getOffers = "Products/GetOffers"
    static let getSearchedProductApi = "Products/search"
    static let applyFilterApi = "Products/Filters"
    static let addReviewApi = "Products/AddReviews"
    static let getReviewsApi = "Products/Reviews"
    static let AddAddressApi = "User_Dashboard/saveAddress"
    static let UpdateAddressApi = "User_Dashboard/updateAddress"
    static let GetAddressApi = "User_Dashboard/getAddress"
    static let DeleteAddressApi = "User_Dashboard/deleteAddress"
    static let UpdateUserProfile = "User_Dashboard/updateUserProfile"
    static let contactUsApi = "Blogs/Addcomments"
    static let ResetPasswordApi = "Authentication/ResetPassword"
    static let paymentBillingApi = "Payment/Billing"
    static let confirmedPaymentApi = "Payment/confirmPayment"
    static let getMyAllInvoiceApi = "Payment/GetMyAllInvoice"
    static let getInvoiceDetailApi = "Payment/GetMyInvoice"
    static let bannerApi = "Products/Banner"
    static let notificatilListApi = "Payment/GetNotificationList"
    
}

struct GAStoryBoard {
    
//    static let PopUp = UIStoryboard.init(name: "FSPopUp", bundle: nil)
//    static let Main = UIStoryboard.init(name: "Main", bundle: nil)
}

//Mark:- DataBase table name key
struct GADataBaseTable  {
   
    //static let Zone = "Zone"
}

struct GAColors {

static let navigationBarColor = UIColor.init(red: 3/255, green: 139/255, blue: 207/255, alpha: 1.0)
    static let themeButtonColor = UIColor.init(red: 251/255, green: 76/255, blue: 97/255, alpha: 1.0)
}

struct GAMessage {

static let networkError = "Internet connection not available !!!"
}

enum APIStatus: String {
    
    case success = "1"
    case NoContent = "204"
    case BadRequest = "400"
    case ServerError = "500"
    case NotFound = "404"
    
}
