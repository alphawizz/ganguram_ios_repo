//
//  GAApiManager.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

enum ApiResult<T, Error> {
    case success(T)
    case error(Error)
}

class GAApiManager: NSObject {
    
    typealias CompletionClosure = (ApiResult<Any,Error>) -> ()
    fileprivate var completionHandler:CompletionClosure!
    typealias completionHandlerForStatus = (_ success:Bool) -> Void
    
    static var shared: GAApiManager = GAApiManager()
    
    private let session: Session = {
        
        let manager = ServerTrustManager(evaluators: ["ganguram.com": DisabledEvaluator()])
        
        let configuration = URLSessionConfiguration.af.default
         return Session(configuration: configuration, serverTrustManager: manager)
        
    }()
    
    
    //MARK: - POST REQUEST
    func POSTRequestWithHeader(strURL: String, Param: [String: Any]? = nil, callback: ((ApiResult<Any, Error>) -> Void)?) {
        self.completionHandler = callback
        var url = "\(GABaseURL)\(strURL)"
        if strURL.contains(GABaseURL) {
            url = strURL
        }
        session.request(url, method: .post, parameters: Param, encoding: JSONEncoding.default, headers: nil).validate().validate(statusCode: 200..<500).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let dict:[String:Any] = value as! [String : Any]
                callback!(.success(dict))
                break
            case .failure(let error):
                // TODO deal with error
                if response.response != nil {
                    _ = (response.response?.statusCode)!
                }
                callback!(.error(error))
            }
        }
    }
    
    func POSTImageWith(strURL: String,img:UIImage, param : [String : String] ,callback: ((ApiResult<Any, Error>) -> Void)?){
        
        self.completionHandler = callback
        let parameters = param
        
        
        let data = img.pngData()
        let url = "\(GABaseURL)\(strURL)"
      //  let headers : HTTPHeaders = ["Authorization" :"Token "]
        
        session.upload(multipartFormData: { (multipartFormData) in
          
          multipartFormData.append(data!, withName: "file", fileName: "file.png", mimeType: "file")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }
            
            
        },to: url, usingThreshold: UInt64.init(), method: .post, headers: nil).response{ response in
            
            if ((response.error == nil)) {
                
                do {
                    if let jsonData = response.data{
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                        print(parsedData)
                        
                        let status = parsedData["Status"] as? NSInteger ?? 0
                        
                        if (status == 1) {
                            if (parsedData["data"] as? [[String: Any]]) != nil {
                            }
                            
                        }else if (status == 2){
                            print("error message")
                        }else{
                            print("error message")
                        }
                    }
                }catch{
                    print("error message")
                }
            }else{
                print(response.error!.localizedDescription)
            }
        }
        
    }
    
    
    func upload(image: Data, to url: Alamofire.URLRequestConvertible, params: [String: Any]) {
        AF.upload(multipartFormData: { multiPart in
            for (key, value) in params {
                if let temp = value as? String {
                    multiPart.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                            if let num = element as? Int {
                                let value = "\(num)"
                                multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            multiPart.append(image, withName: "file", fileName: "file.png", mimeType: "image/png")
        }, with: url)
            .uploadProgress(queue: .main, closure: { progress in
                //Current upload progress of file
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON(completionHandler: { data in
                //Do what ever you want to do with response
            })
    }
    
    
    
    func POSTRequest(strURL: String, Param: [String: Any], callback: ((ApiResult<Any, Error>) -> Void)?) {
        self.completionHandler = callback
        let url = "\(GABaseURL)\(strURL)"
        
        session.request(url, method: .post, parameters: Param, encoding: JSONEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                let dict:[String:Any] = value as! [String : Any]
                callback!(.success(dict))
                break
            case .failure(let error):
                // TODO deal with error
                if response.response != nil {
                    _ = (response.response?.statusCode)!
                }
                callback!(.error(error))
            }
        }
    }
    
    // Post Param as Array
    func post(params:[Any],strURL: String,callback: ((ApiResult<Any, Error>) -> Void)?){
        
        
        let url = "\(GABaseURL)\(strURL)"
        var request = URLRequest(url: try! url.asURL())
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: params)
        
        session.request(request).responseJSON { response in
            switch response.result {
            case .success(let value):
                let dict:[String:Any] = value as! [String : Any]
                callback!(.success(dict))
                break
            case .failure(let error):
                callback!(.error(error))
            }
        }
    }
    
    // Get Request
    func GETRequest(strURL: String,callback: ((ApiResult<Any, Error>) -> Void)?) {
        self.completionHandler = callback
        let url = "\(GABaseURL)\(strURL)"
        
        session.request(url, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success(let value):
                let dict:[String:Any] = value as! [String : Any]
                callback!(.success(dict))
                break
            case .failure(let error):
                callback!(.error(error))
            }
        }
    }
    
    func GETRequestIWithParam(strURL: String, Param: [String: Any], callback: ((ApiResult<Any, Error>) -> Void)?) {
        
        self.completionHandler = callback
        session.request(strURL, method:.get, parameters : Param).responseJSON(completionHandler: { response in
            
            switch(response.result) {
            case .success(_):
                if let data = response.value {
                    let dict:[String:Any] = data as! [String : Any]
                    callback!(.success(dict))
                }
                break
            case .failure(let error):
                callback!(.error(error))
                break
            }
        })
    }
    
    // MARK: - PUT REQUEST
    func PUTRequestWithHeader(strURL: String, Param: [String: Any], callback: ((ApiResult<Any, Error>) -> Void)?) {
        
        self.completionHandler = callback
        let url = "\(GABaseURL)\(strURL)"
        session.request(url, method: .put, parameters: Param, encoding: JSONEncoding.default, headers: nil).validate().validate(statusCode: 200..<500).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let dict:[String:Any] = value as! [String : Any]
                callback!(.success(dict))
                break
            case .failure(let error):
                callback!(.error(error))
            }
        }
    }
    
    // MARK: - DELETE REQUEST
    func delete(strURL: String,Param: [String: Any]? = nil,callback: ((ApiResult<Any, Error>) -> Void)?) {
        
        
        let url = "\(GABaseURL)\(strURL)"
        session.request(url, method: .delete, parameters: Param, encoding: JSONEncoding.default, headers: nil).validate().validate(statusCode: 200..<500).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let dict:[String:Any] = value as! [String : Any]
                callback!(.success(dict))
                break
            case .failure(let error):
                callback!(.error(error))
            }
        }
    }
    
    // MARK: - PATCH REQUEST
    func patch(params:[Any],strURL: String,callback: ((ApiResult<Any, Error>) -> Void)?){
        
        self.completionHandler = callback
        let url = "\(GABaseURL)\(strURL)"
        var request = URLRequest(url: try! url.asURL())
        //some header examples
        
        request.httpMethod = "PATCH"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: params)
        
        session.request(request).responseJSON { response in
            switch response.result {
            case .success(let value):
                let dict:[String:Any] = value as! [String : Any]
                callback!(.success(dict))
                break
            case .failure(let error):
                callback!(.error(error))
            }
        }
    }
    
    func patch(strURL: String, Param: [String: Any], callback: ((ApiResult<Any, Error>) -> Void)?) {
        
        self.completionHandler = callback
        let url = "\(GABaseURL)\(strURL)"
        session.request(url, method: .patch, parameters: Param, encoding: JSONEncoding.default, headers: nil).validate().validate(statusCode: 200..<500).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let dict:[String:Any] = value as! [String : Any]
                callback!(.success(dict))
                break
            case .failure(let error):
                callback!(.error(error))
            }
        }
    }
    
//    func POSTRequestToken(strURL: String, Param: [String: Any], callback: ((ApiResult<Any, Error>) -> Void)?) {
//        self.completionHandler = callback
//
//        session.request(strURL, method: .post, parameters: Param, encoding: JSONEncoding.default).responseJSON { response in
//
//            switch response.result {
//
//            case .success(let value):
//                let dict:[String:Any] = value as! [String : Any]
//                callback!(.success(dict))
//                break
//            case .failure(let error):
//                // TODO deal with error
//                if response.response != nil {
//                    _ = (response.response?.statusCode)!
//                }
//                callback!(.error(error))
//            }
//        }
//    }
    
    //    //Mark -> Call Post Api without parameter
    
    func GETRequestForMessage(strURL: String,Param: [String: Any],completionHandler: @escaping completionHandlerForStatus) {
        
        session.request(strURL, method: .get, parameters: Param, encoding: URLEncoding.default, headers: [:]).responseData { (responsData) in
            
            if responsData.response?.statusCode == 200 {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func upload(icon: Data, image : Data, params: [String: Any]) {
       
        let urlString = "\(GABaseURL)\(GAApi.UpdateUserProfile)"
      //  let headers: HTTPHeaders =
           // ["Content-type": "multipart/form-data",
           // "Accept": "application/json"]
        
        session.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in params {
                    if let temp = value as? String {
            multipartFormData.append(temp.data(using: .utf8)!, withName: key)}

        if let temp = value as? Int {
        multipartFormData.append("(file)".data(using: .utf8)!, withName: key)}

        if let temp = value as? NSArray {
            temp.forEach({ element in
                let keyObj = key + "[]"
                if let string = element as? String {
                    multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                } else
                    if let num = element as? Int {
                        let value = "(num)"
                        multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                }
            })
        }
    }
                multipartFormData.append(icon, withName: "file", fileName: "file.png", mimeType: "image/png")

//                multipartFormData.append(image, withName: "registerImage", fileName: "registerImage.png", mimeType: "registerImage/png")
        },
            to: urlString, //URL Here
            method: .post,
            headers: nil)
            .responseJSON { (resp) in
                defer{}
                print("resp is \(resp)")
        }
    }
    
    
//     func postComplexPictures(url:URL, params:[String:Any],pictures : UIImage, finish: @escaping ((message:String, list:[[String: Any]],isSuccess:Bool)) -> Void) {
//
//        var result:(message:String, list:[[String: Any]],isSuccess:Bool) = (message: "Fail", list:[],isSuccess : false)
//
////        let headers: HTTPHeaders
////        headers = ["Content-type": "multipart/form-data",
////                   "Content-Disposition" : "form-data"]
//
//
//        session.upload(multipartFormData: { (multipartFormData) in
//
//            for (key, value) in params {
//                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
//            }
//                if let imageData = pictures.pngData() {
//                    multipartFormData.append(imageData, withName: "file", fileName: "file.png", mimeType: "image/png")
//            }
//        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: nil).response{ response in
//
//            if((response.error != nil))
//           {
//                do
//                {
//                    if let jsonData = response.data
//                    {
//                        let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
//                        print(parsedData)
//
//                        let status = parsedData["status"] as? NSInteger ?? 0
//                        let msg = parsedData["message"] as? String ?? ""
//
//
//                        if(status==1)
//                        {
//                            result.isSuccess = true
//                            result.message=msg
//                            if let jsonArray = parsedData["data"] as? [[String: Any]] {
//                                result.list=jsonArray
//                            }
//                        }
//                        else
//                        {
//                            result.isSuccess = false
//                            result.message=msg
//                        }
//
//                    }
//                    finish(result)
//                }
//                catch
//                {
//                   finish(result)
//                }
//            }
//            else
//            {
//            finish(result)
//            }
//
//        }
//    }
    
    
// class func callForUploadMultipleImage(url: String, imageToUpload: [UIImage], parameters: [String: Any], completionHandler: @escaping (AnyObject) -> Void) {
//
//    AF.upload(multipartFormData: { multiPart in
//        for (key, value) in params {
//            if let temp = value as? String {
//                multiPart.append(temp.data(using: .utf8)!, withName: key)
//            }
//            if let temp = value as? Int {
//                multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
//            }
//            if let temp = value as? NSArray {
//                temp.forEach({ element in
//                    let keyObj = key + "[]"
//                    if let string = element as? String {
//                        multiPart.append(string.data(using: .utf8)!, withName: keyObj)
//                    } else
//                        if let num = element as? Int {
//                            let value = "\(num)"
//                            multiPart.append(value.data(using: .utf8)!, withName: keyObj)
//                    }
//                })
//            }
//        }
//        multiPart.append(image, withName: "file", fileName: "file.png", mimeType: "image/png")
//    }, with: url)
//        .uploadProgress(queue: .main, closure: { progress in
//            //Current upload progress of file
//            print("Upload Progress: \(progress.fractionCompleted)")
//        })
//        .responseJSON(completionHandler: { data in
//            //Do what ever you want to do with response
//        })
//    }
}


