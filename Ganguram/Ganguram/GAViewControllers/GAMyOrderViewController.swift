//
//  GAMyOrderViewController.swift
//  Ganguram
//
//  Created by Apple on 11/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import KRProgressHUD

class GAMyOrderViewController: UIViewController {
    
    @IBOutlet weak var tableMyOrder: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableMyOrder.register(UINib.init(nibName: String(describing: GAMyOrderTableviewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: GAMyOrderTableviewCell.self))
        
        callAPIForGetAllInvoices()
    }
    
    //Mark -> Call login Api......
       
       func callAPIForGetAllInvoices()  {
           
           if Connectivity.isConnectedToInternet() {
               
            let param : [String : Any] = ["User_id" : GAUser.shared.loginInfoObj.userId!]
               
               KRProgressHUD.show()
               
               GAService.shared.apiForGetAllInvoice(param: param) { (isSucess, message, response) in
                   
                   KRProgressHUD.dismiss()
                   
                   if isSucess! {
                    
                    if let response = response!["data"] as? [[String : Any]] {
                        
                        GAInvoice.shared.saveOfferData(arrayProducts: response)
                    }
                    
                    self.tableMyOrder.reloadData()
                   } else {
                       self.view.makeToast(message)
                   }
               }
           } else {
               self.view.makeToast(GAMessage.networkError)
           }
       }
}
