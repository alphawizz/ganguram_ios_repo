//
//  GACategoryProductsViewController.swift
//  Ganguram
//
//  Created by Apple on 15/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import KRProgressHUD

class GACategoryProductsViewController: UIViewController {

    @IBOutlet weak var collectionviewCategoryProducts: UICollectionView!
    var selctedCategoryDetail : CategoryDetail!
    var arrayAddCardList = [ProductDetail]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        registerXib()
        callGetProductOfSelectedCategoryApi()
        getCardProducts()
    }
    
    func registerXib()  {
        
        collectionviewCategoryProducts.register(UINib(nibName: String(describing: GAProductCollectionviewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: GAProductCollectionviewCell.self))
    }
    
    func getCardProducts()  {
        
        if let objects = UserDefaults.standard.value(forKey: GAUserKey.AddToCard) as? Data {
                         let decoder = JSONDecoder()
                         if let cardList = try? decoder.decode(Array.self, from: objects) as [ProductDetail] {
                            arrayAddCardList = cardList
            }
    }
    }

    
    @IBAction func onProductFilter(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Default Sorting", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.callApplyFilterApi(selectedFilterID: 0)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Popular", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.callApplyFilterApi(selectedFilterID: 1)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Recent", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.callApplyFilterApi(selectedFilterID: 2)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Low To high", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.callApplyFilterApi(selectedFilterID: 3)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "High To Low", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.callApplyFilterApi(selectedFilterID: 4)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func callGetProductOfSelectedCategoryApi()  {
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["id" : selctedCategoryDetail.id!,
                                          "user_id" : GAUser.shared.loginInfoObj.userId!]
            
            KRProgressHUD.show()
            
            GAService.shared.apiForGetProductByCategory(param: param) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    if let response = response!["data"] as? [[String : Any]] {
                        GAProduct.shared.saveProductDetailData(arrayProducts: response)
                        self.collectionviewCategoryProducts.reloadData()
                    }
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
    func callApplyFilterApi(selectedFilterID : Int)  {
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["filter_id" : selectedFilterID,
                                          "user_id" : GAUser.shared.loginInfoObj.userId!
                                          ]
            KRProgressHUD.show()
            
            GAService.shared.apiForApplyFilterApi(param: param) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    if let response = response!["data"] as? [[String : Any]] {
                        GAProduct.shared.saveProductDetailData(arrayProducts: response)
                        self.collectionviewCategoryProducts.reloadData()
                    }
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    

}
