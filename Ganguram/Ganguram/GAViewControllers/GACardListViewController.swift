//
//  GACardListViewController.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit


class GACardListViewController: UIViewController {

    @IBOutlet weak var labelSubtotal: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelCoupanApplied: UILabel!
    @IBOutlet weak var labelShipping: UILabel!
    var productList : [ProductDetail]!
    var totalPrice = Int()
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var tableviewList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         tableviewList.register(UINib.init(nibName: String(describing: GACardTableviewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: GACardTableviewCell.self))
        
         tableviewList.register(UINib.init(nibName: String(describing: GANoDataTableviewCellTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: GANoDataTableviewCellTableViewCell.self))
        
        viewBottom.isHidden = true
        self.getCardProducts()
        if productList != nil {
        tableviewList.tableFooterView = UIView()
        }

    }

    @IBAction func onContinueshopping(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
            let vc = segue.destination as! GAPlacedOrderListViewController
            vc.productList = productList
            let formattedString = self.labelSubtotal.text!.replacingOccurrences(of: "rs", with: "")
            vc.subTotal = formattedString
    }
    
    @IBAction func onProceedToCheckOut(_ sender: Any) {
        
        performSegue(withIdentifier: String.init(describing: GAPlacedOrderListViewController.self), sender: nil)

    }
    
    func getCardProducts()  {
        
        if let objects = UserDefaults.standard.value(forKey: GAUserKey.AddToCard) as? Data {
                         let decoder = JSONDecoder()
                         if let cardList = try? decoder.decode(Array.self, from: objects) as [ProductDetail] {
                            productList = cardList
                            
                            print(productList.first)
                            
                            productList.sort(by: { $0.dateAdded!.compare($1.dateAdded!) == .orderedDescending})

                            for product in productList {
                                
                                print(product.dateAdded!)
                                
                                let intPrice =  Int(product.price!)
                                let productCount = product.productCount
                                let multipleProductPrice = intPrice! * productCount!
                                totalPrice = totalPrice + multipleProductPrice
                            }
                            self.labelSubtotal.text = "\(totalPrice)rs"
                            self.labelShipping.text = "20rs"
                            self.labelCoupanApplied.text = "0rs"
                            self.labelTotal.text = "\(totalPrice + 20)rs"
                            viewBottom.isHidden = false

            }
            viewBottom.isHidden = false
    }
    }
    
}

