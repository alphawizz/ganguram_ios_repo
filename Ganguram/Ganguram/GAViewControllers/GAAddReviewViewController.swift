//
//  GAAddReviewViewController.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import KRProgressHUD
import SDWebImage

class GAAddReviewViewController: UIViewController {

    @IBOutlet weak var labelProductname: UILabel!
    @IBOutlet weak var imageviewProduct: UIImageView!
    @IBOutlet weak var textviewReview: GADesignableTextview!
    @IBOutlet weak var viewRating: FloatRatingView!
    var productID = String()
    var productDetail : ProductDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         loadProductData()
        // Do any additional setup after loading the view.
        textviewReview.placeholder = "Enter your comment"
    }
    
    func loadProductData()  {
        
        labelProductname.text = productDetail.productName
        self.imageviewProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageviewProduct.sd_setImage(with: URL.init(string: productDetail.productImage!)) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                self.imageviewProduct.image = UIImage(named: "imageload")
            } else {
                // Successful in loading image
                self.imageviewProduct.image = image
            }
        }
        
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        
        if isValid() {
        callAPIForAddReview()
        }
    }
    
    func callAPIForAddReview()  {
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["user_id" : GAUser.shared.loginInfoObj.userId!,
                                          "product_id" : productID,
                                          "comments" : textviewReview.text!,
                                          "rating" : viewRating.rating]
            
            KRProgressHUD.show()
            
            GAService.shared.apiForAddReview(param: param) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    self.view.makeToast("Review added sucessfully !!")
                    self.getAllReviews()
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
        
    }
    
    func getAllReviews()  {
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["product_id" : productID]
            
            KRProgressHUD.show()
            
            GAService.shared.apiForGetReviews(param: param) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    if let response = response!["Products"] as? [[String : Any]] {
                        GAReviewList.shared.saveReviewData(arrayProducts: response)
                    }
                    self.navigationController?.popViewController(animated: true)
                       
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
    
    func isValid() -> Bool {
       
        var isValidate = Bool()
        if textviewReview.text == "" {
            isValidate = false
            self.view.makeToast("Please enter your review")
            
        } else if viewRating.rating < 1 {
             isValidate = false
            self.view.makeToast("Please give rating")
        } else {
             isValidate = true
        }
        return isValidate
    }
}
