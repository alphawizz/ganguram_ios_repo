//
//  GAPlacedOrderListViewController.swift
//  Ganguram
//
//  Created by Apple on 21/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class GAPlacedOrderListViewController: UIViewController {

    @IBOutlet weak var tableviewPlacedOrder: UITableView!
    var productList = [ProductDetail]()
     var subTotal = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let objects = UserDefaults.standard.value(forKey: GAUserKey.AddToCard) as? Data {
        let decoder = JSONDecoder()
        if let cardList = try? decoder.decode(Array.self, from: objects) as [ProductDetail] {
           productList = cardList

        // Do any additional setup after loading the view.
        tableviewPlacedOrder.register(UINib.init(nibName: String(describing: GAPlaceOrderTableviewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: GAPlaceOrderTableviewCell.self))
    }
        }
}
    
    @IBAction func onPlaceOrder(_ sender: Any) {
        
        self.performSegue(withIdentifier: "GAOrderPlaceAddressViewController", sender: nil)
    }
    
    @IBAction func onBackToCard(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    if segue.identifier == "GAOrderPlaceAddressViewController" {
               
               let vc = segue.destination as! GAOrderPlaceAddressViewController
               vc.arrayProduct = productList
               vc.subTotal = subTotal
           }
    }
}

