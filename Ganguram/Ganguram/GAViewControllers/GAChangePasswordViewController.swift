//
//  GAChangePasswordViewController.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class GAChangePasswordViewController: UIViewController {

    @IBOutlet weak var textfieldOldPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var textfieldNewPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var textfieldConfirmPassword: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onSave(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
