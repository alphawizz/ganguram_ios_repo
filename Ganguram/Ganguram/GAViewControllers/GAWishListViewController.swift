//
//  GAWishListViewController.swift
//  Ganguram
//
//  Created by Apple on 11/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import KRProgressHUD

class GAWishListViewController: UIViewController {

    @IBOutlet weak var collectionviewWishList: UICollectionView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.registerXib()
        self.callgetWishApi()

        // Do any additional setup after loading the view.
    }
    

   func registerXib()  {
        
        collectionviewWishList.register(UINib(nibName: String(describing: GAWishListProductCollectionview.self), bundle: nil), forCellWithReuseIdentifier: String(describing: GAWishListProductCollectionview.self))
    
        collectionviewWishList.register(UINib(nibName: String(describing: GANoDataCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: GANoDataCollectionViewCell.self))
    }
    
    func callgetWishApi()  {
        
        if Connectivity.isConnectedToInternet() {
            
            KRProgressHUD.show()
            
            let dictData : [String : Any] = ["user_id" : GAUser.shared.loginInfoObj.userId!]
            
            GAService.shared.apiForGetWishList(param: dictData) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    if let response = response!["data"] as? [[String : Any]] {
                        GAWishedProduct.shared.saveWishListData(arrayProducts: response)
                    }
                    
                    self.collectionviewWishList.reloadData()
                } else {
                    
                    GAWishedProduct.shared.arrayWishProducts.removeAll()
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }

}
