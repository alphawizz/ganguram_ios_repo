//
//  GAAddAddressViewController.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KRProgressHUD

class GAAddAddressViewController: UIViewController {

    @IBOutlet weak var textfieldAddress1: SkyFloatingLabelTextField!
      @IBOutlet weak var textfieldAddress2: SkyFloatingLabelTextField!
      @IBOutlet weak var textfieldState: SkyFloatingLabelTextField!
      @IBOutlet weak var textfieldCity: SkyFloatingLabelTextField!
      @IBOutlet weak var textfieldPinCode: SkyFloatingLabelTextField!
    
    @IBOutlet weak var buttonSave: GADesignableButton!
    var addressDetail : ShippingAddress!
    var isFromEditAddress = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadAddressData()
        
    }
    
    func loadAddressData()  {
        
        if isFromEditAddress {
        textfieldAddress1.text = addressDetail.Address1
        textfieldAddress2.text = addressDetail.Address2
        textfieldState.text = addressDetail.State
        textfieldCity.text = addressDetail.City
        textfieldPinCode.text = addressDetail.Pincode
            buttonSave.setTitle("Update Address", for: .normal)
        } else {
            buttonSave.setTitle("Save", for: .normal)
        }
    }

    @IBAction func onSave(_ sender: Any) {
        
        if isValid() {
            if isFromEditAddress {
                callUpdateAddress()
                
            } else {
                callSaveAddress()
            }
        }
    }
    
    func callSaveAddress()  {
        
       if Connectivity.isConnectedToInternet() {
        
        let dictData : [String : Any] = ["address1" : textfieldAddress1.text!,
                                         "address2" : textfieldAddress2.text!,
                                         "state" : textfieldState.text!,
                                         "city" : textfieldCity.text!,
                                         "pincode" : textfieldPinCode.text!,
                                         "user_id" : GAUser.shared.loginInfoObj.userId!]
        
            KRProgressHUD.show()
            
            GAService.shared.apiForAddAddress(param: dictData) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                     self.view.makeToast(message)
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
    
    func callUpdateAddress()  {
        
       if Connectivity.isConnectedToInternet() {
        
        let dictData : [String : Any] = ["address1" : textfieldAddress1.text!,
                                         "address2" : textfieldAddress2.text!,
                                         "state" : textfieldState.text!,
                                         "city" : textfieldCity.text!,
                                         "pincode" : textfieldPinCode.text!,
                                         "user_id" : GAUser.shared.loginInfoObj.userId!,
                                         "address_id" : addressDetail.id!
        ]
        
            KRProgressHUD.show()
            
            GAService.shared.apiForUpdateAddress(param: dictData) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                     self.view.makeToast(message)
                     self.navigationController?.popViewController(animated: true)
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
}

extension GAAddAddressViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldAddress1 {
            if textfieldAddress1.text == "" {
                textfieldAddress1.errorMessage = "Please enter first address"
                
            }
            else {
                textfieldAddress1.errorMessage = ""
            }
            
        } else if textField == textfieldAddress2 {
            
            if textfieldAddress2.text!.count < 0 {
                textfieldAddress2.errorMessage = "Please enter second address"
                
            }
            else {
                textfieldAddress2.errorMessage = ""
            }
        }  else if textField == textfieldState {
            
            if textfieldState.text!.count < 0 {
                textfieldState.errorMessage = "Please enter state"
                
            }
            else {
                textfieldState.errorMessage = ""
            }
        } else if textField == textfieldCity {
            
            if textfieldCity.text!.count < 0 {
                textfieldCity.errorMessage = "Please enter city"
                
            }
            else {
                textfieldCity.errorMessage = ""
            }
        } else if textField == textfieldPinCode {
            
            if textfieldPinCode.text!.count < 0 {
                textfieldPinCode.errorMessage = "Please enter pin code"
                
            }
            else {
                textfieldPinCode.errorMessage = ""
            }
        }
        
        return true
    }
    
    func isValid() -> Bool {
        
        var isValid = Bool()
        
        if textfieldAddress1.text == "" {
            textfieldAddress1.errorMessage = "Please enter first address"
            isValid = false
            
        } else if textfieldAddress2.text == ""  {
            textfieldAddress2.errorMessage = "Please enter second address"
            isValid = false
            
        } else if textfieldState.text == ""  {
            textfieldState.errorMessage = "Please enter state"
            isValid = false
            
        } else if textfieldCity.text == ""  {
            textfieldCity.errorMessage = "Please enter city"
            isValid = false
            
        } else if textfieldPinCode.text == ""  {
            textfieldPinCode.errorMessage = "Please enter pincode"
            isValid = false
            
        } else {
            isValid = true
        }
        
        return isValid
    }
    
}



