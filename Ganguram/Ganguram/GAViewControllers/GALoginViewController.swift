//
//  GALoginViewController.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KRProgressHUD
import Toast_Swift
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit


class GALoginViewController: UIViewController, GIDSignInDelegate {
    
    @IBOutlet weak var textfieldEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldPassword: SkyFloatingLabelTextField!
    var gmailAccessToken = String()
    var fbAccessToken = String()
    
    @IBOutlet weak var buttonFacebook: UIButton!
    @IBOutlet weak var buttonGoogle: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldEmail.delegate = self
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
      //  GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        // Do any additional setup after loading the view.
        
        textfieldEmail.text = "sonali.malviya1702@gmail.com"
        textfieldPassword.text = "12345678"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    //Mark -> IBActions
    
    @IBAction func onLogin(_ sender: Any) {
        
        callAPIForLogin()
    }
    
    @IBAction func onGoogle(_ sender: Any) {
        
        if gmailAccessToken.isEmpty {
            GIDSignIn.sharedInstance()?.signIn()
        }
    }
    
    //Mark -> Login with Google
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                     withError error: Error!) {
        
        if let error = error {
            
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        
        if error == nil {
            let dictParam : [String : Any] = ["name" : user.profile.name!,
                                              "email" : user.profile.email!,
                                              "social" :"1" ]
            
            self.callAPIForSocialLogin(dictData: dictParam)
        }
    }
    
    @IBAction func onFacebook(_ sender: Any) {
        
        if AccessToken.current != nil {
            self.getFBUserData()
            return
        }
        
        let fbLoginManager : LoginManager = LoginManager()
        LoginManager().logOut()
        
        fbLoginManager.logIn(permissions: ["public_profile","email","user_friends"], from: self) { (result, error) -> Void in
            if (error == nil){
                let _ : LoginManagerLoginResult = result!
                
                if (result?.isCancelled)!{
                    return
                }
                self.getFBUserData()
            }
        }
    }
    
    //Mark -> Login with FaceBook

    func getFBUserData(){
        
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, age_range, link, gender, locale, timezone, picture, updated_time, verified, email"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                
                print(result!)
                
                let dict = result as! [String : Any]
                
                self.fbAccessToken =  dict["id"] as! String
                let strFirstName =   dict["name"] as! String
                
                let dictParam : [String : Any] = ["name" : strFirstName,
                                                  "email" : self.fbAccessToken,
                                                  "social" :"2" ]
                
                self.callAPIForSocialLogin(dictData: dictParam)
            }
        })
    }
    
    //Mark -> Call login Api......
    
    func callAPIForLogin()  {
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["email" : textfieldEmail.text!,
                                          "password" : textfieldPassword.text!]
            
            KRProgressHUD.show()
            
            GAService.shared.apiForUserLogin(param: param) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    UserDefaults.standard.removeObject(forKey: GAUserKey.AddToCard)
                    UserDefaults.standard.synchronize()
                    self.navigateToDashBoardScreen()
                    
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
        
    }
    
    // Mark -> Call Api for social Login.....
    
    func callAPIForSocialLogin(dictData : [String : Any])  {
        
        if Connectivity.isConnectedToInternet() {
            
            KRProgressHUD.show()
            
            GAService.shared.apiForSocialLogin(param: dictData) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    UserDefaults.standard.removeObject(forKey: GAUserKey.AddToCard)
                    UserDefaults.standard.synchronize()
                    self.navigateToDashBoardScreen()
                    
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
    func navigateToDashBoardScreen()  {
        
        let main = UIStoryboard(name: "Main", bundle: nil)
        
        let objHome = main.instantiateViewController(withIdentifier: "GADashBoardViewController") as! GADashBoardViewController
        
        let navigationController = NavigationController(rootViewController: objHome)
        let mainViewController = MainViewController()
        
        mainViewController.rootViewController = navigationController
        navigationController.navigationBar.isHidden = false
        
        mainViewController.setup(type: UInt(1))
        
        if #available(iOS 10.0, *) {
            mainViewController.leftViewBackgroundBlurEffect = UIBlurEffect(style: .regular)
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.pushViewController(mainViewController, animated: false)
    }
    
}

extension GALoginViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldEmail {
            if textfieldEmail.text == "" {
                textfieldEmail.errorMessage = "Please enter email address"
                
            } else if !textfieldEmail.isValidEmail(testStr: textfieldEmail.text!) {
                textfieldEmail.errorMessage = "Please enter valid email address"
                
            }
            else {
                textfieldEmail.errorMessage = ""
            }
            
        } else if textField == textfieldPassword {
            if textfieldPassword.text!.count < 0 {
                textfieldPassword.errorMessage = "Please enter password"
                
            }
            else {
                textfieldPassword.errorMessage = ""
            }
        }
        
        return true
    }
    
    func isValid() -> Bool {
        
        var isValid = Bool()
        
        if textfieldEmail.text == "" {
            textfieldEmail.errorMessage = "Please enter email address"
            isValid = false
            
        } else if textfieldPassword.text == ""  {
            textfieldPassword.errorMessage = "Please enter password"
            isValid = false
            
        } else {
            isValid = true
        }
        
        return isValid
    }
    
}
