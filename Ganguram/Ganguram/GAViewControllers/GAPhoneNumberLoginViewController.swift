//
//  GAPhoneNumberLoginViewController.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KRProgressHUD


class GAPhoneNumberLoginViewController: UIViewController, GAVerifyOTPPopupViewControllerDelegate {
    
    @IBOutlet weak var textfieldPhoneNumber: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textfieldPhoneNumber.delegate = self
        textfieldPhoneNumber.text = textfieldPhoneNumber.text
    }
    

    func callApiForGenerateOTP()  {
        
        if Connectivity.isConnectedToInternet() {

            let param : [String : Any] = ["phone" : textfieldPhoneNumber.text!,
                                           ]

                   KRProgressHUD.show()

               GAService.shared.apiForGenerateOTP(param: param) { (isSucess, message, response) in

                    KRProgressHUD.dismiss()

                   if isSucess! {
                    
                      let popupVC = UIStoryboard(name: "PopUpStoryboard", bundle: nil)
                    
                      let reviewVC = popupVC.instantiateViewController(identifier: String.init(describing: GAVerifyOTPPopupViewController.self)) as! GAVerifyOTPPopupViewController
                    reviewVC.modalPresentationStyle = .overFullScreen
                    let otp = response!["otp"] as! Int
                    reviewVC.strOTP = "\(otp)"
                    reviewVC.mobileNumber = self.textfieldPhoneNumber.text ?? ""
                    reviewVC.delegate = self
                      self.present(reviewVC, animated: true, completion: nil)

                   } else {
                       self.view.makeToast(message)
                   }
               }
               } else {
                   self.view.makeToast(GAMessage.networkError)
               }
           }
    
    @IBAction func onBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onContinue(_ sender: Any) {
        
        if isValid() {
        callApiForGenerateOTP()
        }
    }
    
    func getCallWhenOKPressed() {
           
        let main = UIStoryboard(name: "Main", bundle: nil)

        let objHome = main.instantiateViewController(withIdentifier: "GADashBoardViewController") as! GADashBoardViewController

        let navigationController = NavigationController(rootViewController: objHome)
        let mainViewController = MainViewController()

        mainViewController.rootViewController = navigationController
        navigationController.navigationBar.isHidden = false

        mainViewController.setup(type: UInt(1))

        if #available(iOS 10.0, *) {
            mainViewController.leftViewBackgroundBlurEffect = UIBlurEffect(style: .regular)
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.pushViewController(mainViewController, animated: false)
       }
    }


extension GAPhoneNumberLoginViewController : UITextFieldDelegate {

func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)

    if textField == textfieldPhoneNumber {
        
        if textfieldPhoneNumber.text == "" {
            textfieldPhoneNumber.errorMessage = "Please enter phone number"
            
        }  else if newString.count > 10 {
            return false
        }
        else if newString.count == 10 {
            textfieldPhoneNumber.errorMessage = ""
        }
    }
    return true
}
    
    func isValid() -> Bool {
        
        var isValid = Bool()
        
        if textfieldPhoneNumber.text == "" {
            textfieldPhoneNumber.errorMessage = "Please enter phone number"
            isValid = false
            
        }  else {
            isValid = true
        }
        
        return isValid
    }
    
}
