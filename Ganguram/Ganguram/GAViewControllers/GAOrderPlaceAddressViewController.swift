//
//  GAOrderPlaceAddressViewController.swift
//  Ganguram
//
//  Created by Apple on 21/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KRProgressHUD

class GAOrderPlaceAddressViewController: UIViewController {
    
    @IBOutlet weak var textfieldPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldPinCode: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldCity: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldState: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldAddress2: SkyFloatingLabelTextField!
    @IBOutlet weak var textfielsAddress1: SkyFloatingLabelTextField!
    
    var subTotal = String()
    var arrayProduct = [ProductDetail]()
    var invoiceID = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        textfielsAddress1.delegate = self
        textfieldAddress2.delegate = self
        textfieldState.delegate = self
        textfieldCity.delegate = self
        textfieldPinCode.delegate = self
        textfieldPhoneNumber.delegate = self

        if let objects = UserDefaults.standard.value(forKey: GAUserKey.SavedAddress) as? Data {
               let decoder = JSONDecoder()
               if let savedAddress = try? decoder.decode(ShippingAddress.self, from: objects) as ShippingAddress {
                
                textfielsAddress1.text = savedAddress.Address1
                textfieldAddress2.text = savedAddress.Address1
                textfieldState.text = savedAddress.State
                textfieldCity.text = savedAddress.City
                textfieldPinCode.text = savedAddress.Pincode
                textfieldPhoneNumber.text = "1236547896"
           }
               }
    }
    
    @IBAction func onSave(_ sender: Any) {
        
        if isValid() {
        self.callApiForConfirmPaymentAndAddress()
        }
    }
    
    
    func callApiForConfirmPaymentAndAddress()  {
        
        var arraProduct = [[String : Any]]()
        
        for product in arrayProduct {
            
            var dict = [String : Any]()
            
            dict["product_id"] = product.productId
            dict["quantity"] = product.productCount
            
            arraProduct.append(dict)
        }
        
        var product = String()
        
        if JSONSerialization.isValidJSONObject(arraProduct) {
            do{
                let data = try JSONSerialization.data(withJSONObject: arraProduct, options: [])
                if let stringProduct = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    product = stringProduct as String
                }
            }catch{
            }
        }
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["firstname" : GAUser.shared.loginInfoObj.firstName!,
                                          "lastname" : GAUser.shared.loginInfoObj.lastName!,
                                          "address" : textfielsAddress1.text!,
                                          "phone" : textfieldPhoneNumber.text!,
                                          "zip_code" : textfieldPinCode.text!,
                                          "city" : textfieldCity.text!,
                                          "email" : GAUser.shared.loginInfoObj.userEmail!,
                                          "shipping" : "20",
                                          "user_id" : GAUser.shared.loginInfoObj.userId!,
                                          "subtotal" : subTotal,
                                          "products" : product]
            
            KRProgressHUD.show()
            
            GAService.shared.apiForBiling(param: param) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    self.invoiceID = response!["invoice_id"] as! Int
                    self.performSegue(withIdentifier: "GAPaymentOptionViewController", sender: nil)
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "GAPaymentOptionViewController" {
        let vc = segue.destination as! GAPaymentOptionViewController
        vc.invoiceID = self.invoiceID
        }
    }
}


extension GAOrderPlaceAddressViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfielsAddress1 {
            if textfielsAddress1.text == "" {
                textfielsAddress1.errorMessage = "Please enter first address"
                
            }
            else {
                textfielsAddress1.errorMessage = ""
            }
            
        } else if textField == textfieldAddress2 {
            
            if textfieldAddress2.text!.count < 0 {
                textfieldAddress2.errorMessage = "Please enter second address"
                
            }
            else {
                textfieldAddress2.errorMessage = ""
            }
        }  else if textField == textfieldState {
            
            if textfieldState.text!.count < 0 {
                textfieldState.errorMessage = "Please enter state"
                
            }
            else {
                textfieldState.errorMessage = ""
            }
        } else if textField == textfieldCity {
            
            if textfieldCity.text!.count < 0 {
                textfieldCity.errorMessage = "Please enter city"
                
            }
            else {
                textfieldCity.errorMessage = ""
            }
        } else if textField == textfieldPinCode {
            
            if textfieldPinCode.text!.count < 0 {
                textfieldPinCode.errorMessage = "Please enter pin code"
                
            }
            else {
                textfieldPinCode.errorMessage = ""
            }
        } else if textField == textfieldPhoneNumber {
            
            if textfieldPhoneNumber.text!.count < 0 {
                textfieldPhoneNumber.errorMessage = "Please enter phone number"
                
            } else if textfieldPhoneNumber.text!.count < 10 {
                textfieldPhoneNumber.errorMessage = "Phone number must be of 10 digits long"
                
            } else {
                textfieldPhoneNumber.errorMessage = ""
            }
        }
        
        return true
    }
    
    func isValid() -> Bool {
        
        var isValid = Bool()
        
        if textfielsAddress1.text == "" {
            textfielsAddress1.errorMessage = "Please enter first address"
            isValid = false
            
        } else if textfieldAddress2.text == ""  {
            textfieldAddress2.errorMessage = "Please enter second address"
            isValid = false
            
        } else if textfieldState.text == ""  {
            textfieldState.errorMessage = "Please enter state"
            isValid = false
            
        } else if textfieldCity.text == ""  {
            textfieldCity.errorMessage = "Please enter city"
            isValid = false
            
        } else if textfieldPinCode.text == ""  {
            textfieldPinCode.errorMessage = "Please enter pincode"
            isValid = false
            
        } else if textfieldPhoneNumber.text == ""  {
            textfieldPhoneNumber.errorMessage = "Please enter phone number"
            isValid = false
            
        } else {
            isValid = true
        }
        
        return isValid
    }
    
}
