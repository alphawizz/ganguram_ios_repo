//
//  GANotificationListViewController.swift
//  Ganguram
//
//  Created by Apple on 27/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class GANotificationListViewController: UIViewController {

    @IBOutlet weak var tableviewNotificationList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableviewNotificationList.register(UINib.init(nibName: String(describing: GANotificationTableviewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: GANotificationTableviewCell.self))
    }
    
}
