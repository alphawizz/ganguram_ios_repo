//
//  FSCalendarPopupViewController.swift
//  FOSServiceApp
//
//  Created by Mac Mini on 15/10/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import FSCalendar

protocol FSCalendarPopupViewControllerDelegate {
    
    func getAllSelectedDatedFromCalendar(arrDates : [Date])
}


class FSCalendarPopupViewController: UIViewController {
    
    @IBOutlet weak var viewCalendarContainer: UIView!
    @IBOutlet weak var calendar: FSCalendar!
    
    var firstDate: Date?
    var lastDate: Date?
    var datesRange: [Date] = []
    var delegate : FSCalendarPopupViewControllerDelegate!
    var isPlanTourSelected = Bool()
    var nextMonthStartDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCalendarView()
        
       
            calendar.currentPage = Date()
            calendar.scrollEnabled = true
            calendar.pagingEnabled = true
        
    }
    
    
    @IBAction func onCancel(_ sender: Any) {
        
        self.dismiss(animated: false)
    }
    
    @IBAction func onOK(_ sender: Any) {
        
        self.dismiss(animated: false) {
            if self.datesRange.count != 0 {
                
                if let delegate = self.delegate {
                    delegate.getAllSelectedDatedFromCalendar(arrDates: self.datesRange)
                }
            }
        }
    }
}

extension FSCalendarPopupViewController : FSCalendarDataSource, FSCalendarDelegate {
    
    func configureCalendarView()  {
        
        self.calendar.appearance.headerMinimumDissolvedAlpha = 0.0;
        
        if isPlanTourSelected {
            calendar.allowsMultipleSelection = true
        } else {
            calendar.allowsMultipleSelection = false
            if firstDate == nil {
                                      datesRange = [Date()]
                                  }
        }
        calendar.placeholderType = .none
    }
    
  func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
       calendar.today = nil
    
    if isPlanTourSelected {
        
            let today = nextMonthStartDate
            let numberOfDaysInMonth = checkNumberOfDaysInCurrentMonth()
            let nextFiveDays = Calendar.current.date(byAdding: .day, value: numberOfDaysInMonth - 1, to: today)!
            let myRange = datesRange(from: today, to: nextFiveDays)
            for d in myRange {
                calendar.select(d)
            }
            var arrayDate = [Date]()
            for dateObj in myRange {
                 let selectedDate = dateObj.addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT(for: dateObj)))
                arrayDate.append(selectedDate)
            }
            datesRange = arrayDate
            print(arrayDate)
        } else {
            // nothing selected:
            if firstDate == nil {
                let selectedDate = date.addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT(for: date)))
                firstDate = selectedDate
                datesRange = [firstDate!]
                return
            }
            // only first date is selected:
//            if firstDate != nil && lastDate == nil {
//                // handle the case of if the last date is less than the first date:
//                if date <= firstDate! {
//                    calendar.deselect(firstDate!)
//                    firstDate = date
//                    datesRange = [firstDate!]
//                    return
//                }
//                let range = datesRange(from: firstDate!, to: date)
//                lastDate = range.last
//                for d in range {
//                    calendar.select(d)
//                }
//                datesRange = range
//                return
//            }
//            // both are selected:
//            if firstDate != nil && lastDate != nil {
//                for d in calendar.selectedDates {
//                    calendar.deselect(d)
//                }
//                lastDate = nil
//                firstDate = nil
//                datesRange = []
//                if firstDate == nil {
//                    firstDate = date
//                    datesRange = [firstDate!]
//                    let range = datesRange(from: firstDate!, to: date)
//                    for d in range {
//                        calendar.select(d)
//                    }
//                    datesRange = range
//                    return
//                }
//            }
        }
    }
    
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // both are selected:
        
        // NOTE: the is a REDUANDENT CODE:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            datesRange = []
        }
    }
    
    func checkNumberOfDaysInCurrentMonth() -> Int  {
        
        let calendar = Calendar.current
        let date = nextMonthStartDate
        
        // Calculate start and end of the current year (or month with `.month`):
        let interval = calendar.dateInterval(of: .month, for: date)! //change year it will no of days in a year , change it to month it will give no of days in a current month
        
        // Compute difference in days:
        let days = calendar.dateComponents([.day], from: interval.start, to: interval.end).day!
        print(days)
        return days
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        let curDate = Date().addingTimeInterval(-24*60*60)
        if date > curDate {
            return false
        } else {
            return true
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        let curDate = Date().addingTimeInterval(-24*60*60)
        if date > curDate {
            return UIColor.gray
        } else {
            return UIColor.black
        }
    }
    
}

