//
//  GAVerifyOTPPopupViewController.swift
//  Ganguram
//
//  Created by Apple on 14/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import KRProgressHUD

protocol GAVerifyOTPPopupViewControllerDelegate {
    func getCallWhenOKPressed()
}

class GAVerifyOTPPopupViewController: UIViewController {

    @IBOutlet weak var labelOtp1: UILabel!
    @IBOutlet weak var labelOTP2: UILabel!
    @IBOutlet weak var labelOTP4: UILabel!
    @IBOutlet weak var labelOTP3: UILabel!
    
    var strOTP = String()
    var mobileNumber = String()
    var delegate : GAVerifyOTPPopupViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let character = strOTP.character(at: 0) {
           labelOtp1.text = "\(character)"
        }
        
        if let character = strOTP.character(at: 1) {
           labelOTP2.text = "\(character)"
        }
        
        if let character = strOTP.character(at: 2) {
           labelOTP3.text = "\(character)"
        }
        
        if let character = strOTP.character(at: 3) {
           labelOTP4.text = "\(character)"
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onOK(_ sender: Any) {
        
        self.callOTPVerifyApi()
        
    }
    
    func callOTPVerifyApi()  {
        
        if Connectivity.isConnectedToInternet() {

                   let param : [String : Any] = ["otp" : strOTP,
                                                 "mobile" : mobileNumber]

                          KRProgressHUD.show()

                      GAService.shared.apiForVerifyOTP(param: param) { (isSucess, message, response) in

                           KRProgressHUD.dismiss()

                          if isSucess! {
                            
                            self.dismiss(animated: false) {
                                if let delegate = self.delegate {
                                    delegate.getCallWhenOKPressed()
                                }
                            }

                          } else {
                              self.view.makeToast(message)
                          }
                      }
                      } else {
                          self.view.makeToast(GAMessage.networkError)
                      }
    }
   
}
