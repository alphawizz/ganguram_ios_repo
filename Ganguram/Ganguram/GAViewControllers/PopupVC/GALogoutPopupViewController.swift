//
//  GALogoutPopupViewController.swift
//  Ganguram
//
//  Created by Apple on 27/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

protocol GALogoutPopupViewControllerDelegate : AnyObject {
    
    func onYesOfLogoutPopup()
}


class GALogoutPopupViewController: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    weak var delegate: GALogoutPopupViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onNo(_ sender: Any) {
        
        self.dismiss(animated: false)
    }
    
    @IBAction func onYes(_ sender: Any) {
        
        self.dismiss(animated: false) {
            
            if let delegte = self.delegate {
                delegte.onYesOfLogoutPopup()
            }
        }
    }
}
