//
//  GAContactUsViewController.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KRProgressHUD
import UITextView_Placeholder

class GAContactUsViewController: UIViewController {

    @IBOutlet weak var textfieldName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldSubject: SkyFloatingLabelTextField!
    
    @IBOutlet weak var textviewMessage: GADesignableTextview!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textfieldName.delegate = self
        textfieldEmail.delegate = self
        textfieldSubject.delegate = self
        textviewMessage.placeholder = "Enter your message"
        
    }

    @IBAction func onSendMessage(_ sender: Any) {
        if isValid() {
            callApiforContactUs()
        }
    }
    
    func callApiforContactUs() {
        
        if Connectivity.isConnectedToInternet() {
            
            let dictData : [String : Any] = ["name" : textfieldName.text!,
                                             "comment" : textviewMessage.text!,
                                             "user_id" : GAUser.shared.loginInfoObj.userId!,
                                             "email" : textfieldEmail.text!]
                    
                   KRProgressHUD.show()
                   
                   GAService.shared.apiForContactUs(param: dictData) { (isSucess, message, response) in
                       
                       KRProgressHUD.dismiss()
                       
                       if isSucess! {
                          
                           
                       } else {
                           self.view.makeToast(message)
                       }
                   }
               } else {
                   self.view.makeToast(GAMessage.networkError)
               }
    }
}

extension GAContactUsViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textfieldName {
            if textfieldName.text!.count < 0 {
                textfieldName.errorMessage = "Please enter name"
                
            }
            else {
                textfieldName.errorMessage = ""
                
            }
        } else if textField == textfieldEmail {
            if textfieldEmail.text == "" {
                textfieldEmail.errorMessage = "Please enter email address"
                
            } else if !textfieldEmail.isValidEmail(testStr: textfieldEmail.text!) {
                textfieldEmail.errorMessage = "Please enter valid email address"
                
            }
            else {
                textfieldEmail.errorMessage = ""
            }
            
        }  else if textField == textfieldSubject {
            if textfieldSubject.text!.count < 0 {
                textfieldSubject.errorMessage = "Please enter subject"
                
            }
            else {
                textfieldSubject.errorMessage = ""
            }
        }
        
        return true
    }
    
    func isValid() -> Bool {
        
        var isValid = Bool()
        
        if textfieldName.text == "" {
            textfieldName.errorMessage = "Please enter name"
            isValid = false
            
        } else if textfieldEmail.text == "" {
            textfieldEmail.errorMessage = "Please enter email address"
            isValid = false
            
        } else if textfieldSubject.text == ""  {
            textfieldSubject.errorMessage = "Please enter subject"
            isValid = false
            
        } else if textviewMessage.text == ""  {
            textfieldSubject.errorMessage = "Please enter message"
            isValid = false
            
        } else {
            isValid = true
        }
        
        return isValid
    }
    
}


