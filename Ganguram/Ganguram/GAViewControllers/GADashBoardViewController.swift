//
//  GADashBoardViewController.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import DLAutoSlidePageViewController
import KRProgressHUD

class GADashBoardViewController: UIViewController, FloatRatingViewDelegate {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionviewList: UICollectionView!
    let ImageViewControllerIdentifier = "GAPagingImageViewController"
    
    @IBOutlet weak var labelAddToCardCount: UILabel!
    @IBOutlet weak var buttonLocation: UIButton!
    @IBOutlet weak var imageviewHome: UIImageView!
    @IBOutlet weak var buttonHome: UIButton!
    
    @IBOutlet weak var labelNotficationCount: UILabel!
    @IBOutlet weak var imageviewOffer: UIImageView!
    @IBOutlet weak var buttonOffer: UIButton!
    
    @IBOutlet weak var imageviewSearch: UIImageView!
    @IBOutlet weak var buttonSearch: UIButton!
    
    @IBOutlet weak var imageviewProfile: UIImageView!
    @IBOutlet weak var buttonProfile: UIButton!
    
    @IBOutlet weak var buttonAddToCard: UIButton!
    
    @IBOutlet weak var buttonNotification: UIButton!
    
    let dropDownLocation = DropDown()
    let arrStates = ["Kolkata","MetroCity","All India"]
    var selectedLocationID = Int()
    var arrayAddCardList = [ProductDetail]()
    var arrayBannerImages = [String]()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.buttonLocation.setTitle("Kolkata", for: .normal)
        selectedLocationID = 2
        
        registerXib()
        setBottomViewProperties()
        self.configureDropDown()
        dropDowenHendeler()
        callApiForBanner()
        
        labelAddToCardCount.layer.cornerRadius = 7
        labelAddToCardCount.layer.masksToBounds = true
        
        labelNotficationCount.layer.cornerRadius = 7
        labelNotficationCount.layer.masksToBounds = true
        
       DispatchQueue.global(qos: .background).async {

        self.callNotificationListApi()
        
            DispatchQueue.main.async {
                print("This is run on the main queue, after the previous code in outer block")
            }
        }
              
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        callGetProductsApi()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    
       
        super.viewWillDisappear(true)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func registerXib()  {
        
        collectionviewList.register(UINib(nibName: String(describing: GAProductCollectionviewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: GAProductCollectionviewCell.self))
    }
    
    func callApiForBanner()  {
        
         if Connectivity.isConnectedToInternet() {
                   
                   KRProgressHUD.show()
                   
                   GAService.shared.apiForBanner { (isSucess, message, response) in
                       
                       KRProgressHUD.dismiss()
                       
                       if isSucess! {
                             if let response = response!["data"] as? [[String : Any]] {
                                
                                for dictImage in response {
                                    
                                    if let bannerImage = dictImage["product_url"] as? String {
                                        let banner =  GABaseUrlBanner + bannerImage
                                        self.arrayBannerImages.append(banner)
                                    }
                                }
                                
                                self.setupElements()
                           }
                       } else {
                           self.view.makeToast(message)
                       }
                   }
               } else {
                   self.view.makeToast(GAMessage.networkError)
               }
        
    }
    
    func configureDropDown()  {
        
        dropDownLocation.dataSource = arrStates
        dropDownLocation.anchorView = buttonLocation
        dropDownLocation.bottomOffset = CGPoint(x: 0, y: 38)
        dropDownLocation.offsetFromWindowBottom = 40
    }
    
    func dropDowenHendeler() {
        
        dropDownLocation.selectionAction = { [unowned self] (index: Int, item: String) in
            
            let selectedValue = self.arrStates[index]
            self.buttonLocation.setTitle(selectedValue, for: .normal)
            
            switch index {
            case 0:
                self.selectedLocationID = 2
                self.callGetProductsApi()
                break
            case 1:
                self.selectedLocationID = 8
                self.callGetProductsApi()
                break
            case 2:
                self.selectedLocationID = 9
                self.callGetProductsApi()
                break
           
            default:
                break
            }
        }
    }
    
    @IBAction func onDropDownLocation(_ sender: Any) {
         self.dropDownLocation.show()
        
    }
    @IBAction func onLocation(_ sender: Any) {
        self.dropDownLocation.show()
    }
    
    @IBAction func onMenu(_ sender: Any) {
        
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func onHome(_ sender: Any) {
        
        imageviewHome.image = UIImage.init(named: "redhome")
        buttonHome.setTitleColor(GAColors.themeButtonColor, for: .normal)
        
        imageviewOffer.image = UIImage.init(named: "offer")
        buttonOffer.setTitleColor(UIColor.white, for: .normal)
        
        imageviewSearch.image = UIImage.init(named: "black_search")
        buttonSearch.setTitleColor(UIColor.white, for: .normal)
        
        imageviewProfile.image = UIImage.init(named: "black_user")
        buttonProfile.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func onOffer(_ sender: Any) {
    }
    
    @IBAction func onSearch(_ sender: Any) {
    }
    
    @IBAction func onProfile(_ sender: Any) {
    }
    
    func setBottomViewProperties()  {
        
        imageviewHome.image = UIImage.init(named: "redhome")
               buttonHome.setTitleColor(UIColor.red, for: .normal)
               
               imageviewOffer.image = UIImage.init(named: "offer")
               buttonOffer.setTitleColor(UIColor.darkGray, for: .normal)
               
               imageviewSearch.image = UIImage.init(named: "black_search")
               buttonSearch.setTitleColor(UIColor.darkGray, for: .normal)
               
               imageviewProfile.image = UIImage.init(named: "black_user")
               buttonProfile.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    func callGetProductsApi()  {
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["location_id" : selectedLocationID,
                                          "user_id" : GAUser.shared.loginInfoObj.userId!]
            
            KRProgressHUD.show()
            
            GAService.shared.apiForGetProductListOfSelectedLocation(param: param) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    if let response = response!["data"] as? [[String : Any]] {
                        GAProduct.shared.saveProductDetailData(arrayProducts: response)
                        self.collectionviewList.reloadData()
                    }
                    
                    self.getCardProducts()
                    self.collectionviewList.reloadData()
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
    
    func callNotificationListApi()  {
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["user_id" : GAUser.shared.loginInfoObj.userId!]
            
            KRProgressHUD.show()
            
            GAService.shared.apiForNotificationList(param: param) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                  if let response = response!["data"] as? [[String : Any]] {
                    GANotification.shared.saveNotificationData(arrayProducts: response)
                 }
                    self.labelNotficationCount.text = "\(GANotification.shared.arrayNotificationList.count)"
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
    func getCardProducts()  {
        
        if let objects = UserDefaults.standard.value(forKey: GAUserKey.AddToCard) as? Data {
                         let decoder = JSONDecoder()
                         if let cardList = try? decoder.decode(Array.self, from: objects) as [ProductDetail] {
                            arrayAddCardList = cardList
            }
    }
        labelAddToCardCount.text = "\(arrayAddCardList.count)"
    }
    
}

extension GADashBoardViewController {
    
    // MARK: - Private
    
    private func setupElements() {
        setupPageViewController()
    }
    
    private func setupPageViewController() {
        let pages: [UIViewController] = setupPages()
        let pageViewController = DLAutoSlidePageViewController(pages: pages,
                                                               timeInterval: 3.0,
                                                               transitionStyle: .scroll,
                                                               interPageSpacing: 0.0)
        addChild(pageViewController)
        containerView.addSubview(pageViewController.view)
        pageViewController.view.frame = containerView.bounds
    }
    
    private func setupPages() -> [UIViewController] {
        
        var arrayVC = [UIViewController]()
        
        for images in arrayBannerImages {
            
            let page1 = storyboard?.instantiateViewController(withIdentifier: ImageViewControllerIdentifier) as! GAPagingImageViewController
            page1.setupElements(image: images )
            arrayVC.append(page1)
        }

        return arrayVC
    }
}
