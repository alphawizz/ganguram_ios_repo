//
//  GAProductListViewController.swift
//  Ganguram
//
//  Created by Apple on 11/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import KRProgressHUD

class GAProductListViewController: UIViewController
{
    @IBOutlet weak var collectionviewProductList: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerXib()
        callGetAllCategoryApi()
    }
    
    func registerXib()  {
        
        collectionviewProductList.register(UINib(nibName: String(describing: GAProductWithOnlyNameCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: GAProductWithOnlyNameCollectionViewCell.self))
    }
    
    func callGetAllCategoryApi()  {
        
        if Connectivity.isConnectedToInternet() {
            
            KRProgressHUD.show()
            
            GAService.shared.apiForGetAllCategoryList { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    if let response = response!["data"] as? [[String : Any]] {
                        GACategory.shared.saveCategoryData(arrayProducts: response)
                    }
                    self.collectionviewProductList.reloadData()
                    
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
}
