//
//  GAPagingImageViewController.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage

class GAPagingImageViewController: UIViewController {
    
    @IBOutlet weak public var logoImageView: UIImageView!
    
    var logoImage: String?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
      super.viewDidLoad()
     
        logoImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        logoImageView.sd_setImage(with: URL.init(string: logoImage!)) { (image, error, cache, urls) in
                   if (error != nil) {
                       // Failed to load image
                       self.logoImageView.image = UIImage(named: "user")
                   } else {
                       // Successful in loading image
                       self.logoImageView.image = image
                   }
               }
    }
    
    // MARK: - Public
    
    func setupElements(image: String) {
      logoImage = image
       
    }

}
