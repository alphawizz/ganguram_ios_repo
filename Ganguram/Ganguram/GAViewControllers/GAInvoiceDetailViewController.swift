//
//  GAInvoiceDetailViewController.swift
//  Ganguram
//
//  Created by Apple on 21/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import KRProgressHUD

class GAInvoiceDetailViewController: UIViewController {

    @IBOutlet weak var imageviewUser: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelOrderNumber: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var tableviewProductList: UITableView!
    var selectedInvoiceID = String()
    var invoiceDetail : InvoiceDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loadUserData()
        self.callGetInvoiceDetailApi()
        // Do any additional setup after loading the view.
        tableviewProductList.register(UINib.init(nibName: String(describing: GAPlaceOrderTableviewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: GAPlaceOrderTableviewCell.self))
    }
    
    func loadUserData()  {
        
        labelEmail.text = GAUser.shared.loginInfoObj.userEmail
        labelUserName.text = GAUser.shared.loginInfoObj.userFullname
        labelAmount.text = invoiceDetail.totalPrice
        labelAddress.text = invoiceDetail.delivery_address
        labelOrderNumber.text = invoiceDetail.sale_item_id
        
        imageviewUser.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageviewUser.sd_setImage(with: URL.init(string: GAUser.shared.loginInfoObj.userImage!)) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                self.imageviewUser.image = UIImage(named: "user")
            } else {
                // Successful in loading image
                self.imageviewUser.image = image
            }
        }
    }
    
    func callGetInvoiceDetailApi()  {
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["Invoice_id" : selectedInvoiceID]
            
            KRProgressHUD.show()
            
            GAService.shared.apiForInvoiceDetail(param: param) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
               
                if isSucess! {
                    if let response = response!["data"] as? [[String : Any]] {
                                       GAInvoiceProducts.shared.saveInvoiceProductData(arrayProducts: response)
                                       self.tableviewProductList.reloadData()
                                   }
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
}
