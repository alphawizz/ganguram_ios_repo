//
//  GAForgotPasswordViewController.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KRProgressHUD

class GAForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var textfieldEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var buttonForgotPassword: GADesignableButton!
    
    var isFromChangePassword = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        
        if isFromChangePassword {
            self.navigationItem.title = "Change Password"
            
        } else {
            self.navigationItem.title = "Forgot Password"
        }
    }
    

    @IBAction func onResetPassword(_ sender: Any) {
        
        if isValid() {
            callAPIResetPassword()
        }
    }
    
    func callAPIResetPassword()  {
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["email" : textfieldEmail.text!
                                          ]
            
            KRProgressHUD.show()
            
            GAService.shared.apiForResetPassword(param: param) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    
                    self.view.makeToast(response!["message"] as? String)
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
        
    }

}

extension GAForgotPasswordViewController : UITextFieldDelegate {

func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if textField == textfieldEmail {
        if textfieldEmail.text == "" {
            textfieldEmail.errorMessage = "Please enter email address"
            
        } else if !textfieldEmail.isValidEmail(testStr: textfieldEmail.text!) {
            textfieldEmail.errorMessage = "Please enter valid email address"
            
        }
        else {
            textfieldEmail.errorMessage = ""
        }
        
    }
    return true
}
    
    func isValid() -> Bool {
        
        var isValid = Bool()
        
        if textfieldEmail.text == "" {
            textfieldEmail.errorMessage = "Please enter email address"
            isValid = false
            
        }  else {
            isValid = true
        }
        
        return isValid
    }
}
