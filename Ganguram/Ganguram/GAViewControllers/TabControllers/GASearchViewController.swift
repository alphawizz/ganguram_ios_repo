//
//  GASearchViewController.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class GASearchViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionviewSearchResult: UICollectionView!
    
    @IBOutlet weak var imageviewHome: UIImageView!
    @IBOutlet weak var buttonHome: UIButton!
    
    @IBOutlet weak var imageviewOffer: UIImageView!
    @IBOutlet weak var buttonOffer: UIButton!
    
    @IBOutlet weak var imageviewSearch: UIImageView!
    @IBOutlet weak var buttonSearch: UIButton!
    
    @IBOutlet weak var imageviewProfile: UIImageView!
    @IBOutlet weak var buttonProfile: UIButton!
    var arrayAddCardList = [ProductDetail]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBottomViewProperties()
        registerXib()
        searchBar.delegate = self
        searchBar.returnKeyType = .done
        getCardProducts()
        
        // Do any additional setup after loading the view.
    }
    
    func registerXib()  {
        
        collectionviewSearchResult.register(UINib(nibName: String(describing: GASearchCollectionviewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: GASearchCollectionviewCell.self))
        
        collectionviewSearchResult.register(UINib(nibName: String(describing: GASearchProductResultCollectionviewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: GASearchProductResultCollectionviewCell.self))
    }
    
    func setBottomViewProperties()  {
        
        imageviewHome.image = UIImage.init(named: "black_home")
        buttonHome.setTitleColor(UIColor.darkGray, for: .normal)
        
        imageviewOffer.image = UIImage.init(named: "offer")
        buttonOffer.setTitleColor(UIColor.darkGray, for: .normal)
        
        imageviewSearch.image = UIImage.init(named: "red_search")
        buttonSearch.setTitleColor(GAColors.themeButtonColor, for: .normal)
        
        imageviewProfile.image = UIImage.init(named: "black_user")
        buttonProfile.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func getCardProducts()  {
           
           if let objects = UserDefaults.standard.value(forKey: GAUserKey.AddToCard) as? Data {
                            let decoder = JSONDecoder()
                            if let cardList = try? decoder.decode(Array.self, from: objects) as [ProductDetail] {
                               arrayAddCardList = cardList
               }
       }
           
       }
}

extension GASearchViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        callAPIForSearchText(text : text)
        return true
    }
    
    func callAPIForSearchText(text : String)  {
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["content" : text]
            
            GAService.shared.apiForGetSearchedProduct(param: param) { (isSucess, message, response) in
                
                if isSucess! {
                    if let response = response!["data"] as? [[String : Any]] {
                        GAProduct.shared.saveProductDetailData(arrayProducts: response)
                        self.collectionviewSearchResult.reloadData()
                    }
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
}
