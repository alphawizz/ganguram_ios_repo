//
//  GAOfferViewController.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import KRProgressHUD

class GAOfferViewController: UIViewController {

    @IBOutlet weak var collectionviewOffer: UICollectionView!
    @IBOutlet weak var imageviewHome: UIImageView!
      @IBOutlet weak var buttonHome: UIButton!
      
      @IBOutlet weak var imageviewOffer: UIImageView!
      @IBOutlet weak var buttonOffer: UIButton!
      
      @IBOutlet weak var imageviewSearch: UIImageView!
      @IBOutlet weak var buttonSearch: UIButton!
      
      @IBOutlet weak var imageviewProfile: UIImageView!
      @IBOutlet weak var buttonProfile: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setBottomViewProperties()
        registerXib()
        callGetOfferApi()
    }
    
    func registerXib()  {
           
           collectionviewOffer.register(UINib(nibName: String(describing: GAOfferCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: GAOfferCollectionViewCell.self))
       }
   
    
    func setBottomViewProperties()  {
        imageviewHome.image = UIImage.init(named: "black_home")
               buttonHome.setTitleColor(UIColor.darkGray, for: .normal)
               
               imageviewOffer.image = UIImage.init(named: "red_offer")
               buttonOffer.setTitleColor(GAColors.themeButtonColor, for: .normal)
               
               imageviewSearch.image = UIImage.init(named: "black_search")
               buttonSearch.setTitleColor(UIColor.darkGray, for: .normal)
               
               imageviewProfile.image = UIImage.init(named: "black_user")
               buttonProfile.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    func callGetOfferApi()  {
        
        if Connectivity.isConnectedToInternet() {
                   
                   KRProgressHUD.show()
                   
                   GAService.shared.apiForGetOffers { (isSucess, message, response) in
                       
                       KRProgressHUD.dismiss()
                       
                       if isSucess! {
                             if let response = response!["Product"] as? [[String : Any]] {
                           GAOffer.shared.saveOfferData(arrayProducts: response)
                           }
                        self.collectionviewOffer.reloadData()
                           
                       } else {
                           self.view.makeToast(message)
                       }
                   }
               } else {
                   self.view.makeToast(GAMessage.networkError)
               }
        
    }

}
