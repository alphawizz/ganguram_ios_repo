//
//  GAProfileViewController.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KRProgressHUD
import SDWebImage

class GAProfileViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var textfieldName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldMobileNumber: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tableviewAddress: UITableView!
    @IBOutlet weak var imageviewProfile: UIImageView!
    
    @IBOutlet weak var imageviewHome: UIImageView!
    @IBOutlet weak var buttonHome: UIButton!
    
    @IBOutlet weak var imageviewOffer: UIImageView!
    @IBOutlet weak var buttonOffer: UIButton!
    
    @IBOutlet weak var imageviewSearch: UIImageView!
    @IBOutlet weak var buttonSearch: UIButton!
    
    @IBOutlet weak var imageviewTabProfile: UIImageView!
    @IBOutlet weak var buttonProfile: UIButton!
    
    @IBOutlet weak var buttonSave: UIButton!
    var profileUpdateUrl : URL!
    var oldProfileImage = UIImage()
    var addressID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        registerXib()
        tableviewAddress.estimatedRowHeight = 60
        tableviewAddress.rowHeight = UITableView.automaticDimension
        setBottomViewProperties()
        tableviewAddress.tableFooterView = UIView()
        
        textfieldName.delegate = self
        textfieldMobileNumber.delegate = self
        textfieldEmail.delegate = self
        loadUserData()
        getSavedAddress()
        
        self.buttonSave.backgroundColor = UIColor.lightGray
        self.buttonSave.isUserInteractionEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callGetAddress()
        tableviewAddress.reloadData()
    }
    
    func registerXib()  {
        
        tableviewAddress.register(UINib.init(nibName: String(describing: GAAddressTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: GAAddressTableViewCell.self))
        
        tableviewAddress.register(UINib.init(nibName: String(describing: GANoDataTableviewCellTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: GANoDataTableviewCellTableViewCell.self))
    }
    
    func loadUserData()  {
        
        textfieldName.text = GAUser.shared.loginInfoObj.userFullname
        textfieldEmail.text = GAUser.shared.loginInfoObj.userEmail
        textfieldMobileNumber.text = GAUser.shared.loginInfoObj.userPhone
        
        imageviewProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageviewProfile.sd_setImage(with: URL.init(string: GAUser.shared.loginInfoObj.userImage!)) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                self.imageviewProfile.image = UIImage(named: "user")
                self.oldProfileImage = UIImage(named: "user")!
            } else {
                // Successful in loading image
                self.imageviewProfile.image = image
                self.oldProfileImage = image!
            }
        }
    }
    
    func checkAnyChangesPerformed() -> Bool {
        
        if GAUser.shared.loginInfoObj.userEmail == textfieldEmail.text && GAUser.shared.loginInfoObj.userPhone == textfieldMobileNumber.text && GAUser.shared.loginInfoObj.userFullname == textfieldName.text &&  (self.oldProfileImage == UIImage(named: "user")! || self.oldProfileImage.isEqual(self.imageviewProfile.image!)) {
            return false
        } else {
            
            return true
        }
    }
    
    @IBAction func onSave(_ sender: Any) {
        
        if checkAnyChangesPerformed() {
        
        let imageData =   imageviewProfile.image!.jpegData(compressionQuality: 0.1)!
        
        let base64String = imageData.base64EncodedString()
        
        let dictData : [String : String] = ["user_id" : GAUser.shared.loginInfoObj.userId!, "name" : textfieldName.text!, "email" : textfieldEmail.text!, "number" : "7415558856" , "fileImage" : base64String]
        
        if Connectivity.isConnectedToInternet() {
            
            KRProgressHUD.show()
            
            GAService.shared.apiForUpdateProfile(param: dictData) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    
                    let responseData = response!["data"] as! [String : Any]
                    
                    let response : [String : Any] = ["data" :["user_id" : GAUser.shared.loginInfoObj.userId!,
                                                     "created" : GAUser.shared.loginInfoObj.created!,
                                                     "firstname" : GAUser.shared.loginInfoObj.firstName!,
                                                     "house_no" : GAUser.shared.loginInfoObj.houseNo!,
                                                     "lastname" : GAUser.shared.loginInfoObj.lastName!,
                                                     "mobile_verified" : GAUser.shared.loginInfoObj.mobileVerified!,
                                                     "modified" : GAUser.shared.loginInfoObj.modified!,
                                                     "pincode" : GAUser.shared.loginInfoObj.pincode!,
                                                     "reg_code" : GAUser.shared.loginInfoObj.regCode!,
                                                     "rewards" : GAUser.shared.loginInfoObj.rewards!,
                                                     "socity_id" : GAUser.shared.loginInfoObj.socityId!,
                                                     "status" : GAUser.shared.loginInfoObj.status!,
                                                     "user_bdate" : GAUser.shared.loginInfoObj.userBdate!,
                                                     "user_city" : GAUser.shared.loginInfoObj.userCity!,
                                                     "user_email" : responseData["user_email"] as? String ?? "",
                                                     "user_fullname" : responseData["user_fullname"] as? String ?? "",
                                                     "user_gcm_code" : GAUser.shared.loginInfoObj.userGcmCode!,
                                                     "user_image" : responseData["user_image"] as? String ?? "",
                                                     "user_ios_token" : GAUser.shared.loginInfoObj.userIosToken!,
                                                     "user_password" : GAUser.shared.loginInfoObj.userPassword!,
                                                     "user_phone" : responseData["user_phone"] as? String ?? "",
                                                     "varification_code" : GAUser.shared.loginInfoObj.varificationCode!,
                                                     "varified_token" : GAUser.shared.loginInfoObj.varifiedToken!,
                                                     "wallet" : GAUser.shared.loginInfoObj.wallet!]]
                    
                    do {
                        
                        UserDefaults.standard.removeObject(forKey: GAUserKey.LoginData)

                        let encodedData = try NSKeyedArchiver.archivedData(withRootObject: response, requiringSecureCoding: true)
                        
                        GADefault.setValue(encodedData, forKey: GAUserKey.LoginData)
                        GADefault.synchronize()
                        GAUser.shared.getUserInfo()
                        
                        NotificationCenter.default.post(name: Notification.Name("refreshUserProfileData"), object: nil, userInfo: nil)

                        self.view.makeToast("Profile updated sucessfully!!")
                        
                        self.buttonSave.backgroundColor = UIColor.lightGray
                        self.buttonSave.isUserInteractionEnabled = false
                        
                    } catch {
                        print(error.localizedDescription)
                        // or display a dialog
                    }
                    
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    }
    
    @IBAction func onAddAddress(_ sender: Any) {
        
    }
    
    @IBAction func onCamera(_ sender: Any) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionsheet = UIAlertController(title: "Photo Source", message: "Choose A Source", preferredStyle: .actionSheet)
        
        actionsheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }else
            {
                print("Camera is Not Available")
            }
            
        }))
        actionsheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction)in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        actionsheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionsheet,animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageviewProfile.image = image
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func setBottomViewProperties()  {
        
        imageviewHome.image = UIImage.init(named: "black_home")
        buttonHome.setTitleColor(UIColor.darkGray, for: .normal)
        
        imageviewOffer.image = UIImage.init(named: "offer")
        buttonOffer.setTitleColor(UIColor.darkGray, for: .normal)
        
        imageviewSearch.image = UIImage.init(named: "black_search")
        buttonSearch.setTitleColor(UIColor.darkGray, for: .normal)
        
        imageviewTabProfile.image = UIImage.init(named: "red_user")
        buttonProfile.setTitleColor(GAColors.themeButtonColor, for: .normal)
    }
    
    func callGetAddress()  {
        
        if Connectivity.isConnectedToInternet() {
            
            let dictData : [String : Any] = ["user_id" : GAUser.shared.loginInfoObj.userId!]
            
            KRProgressHUD.show()
            
            GAService.shared.apiForGetAddress(param: dictData) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    
                    if let response = response!["data"] as? [[String : Any]] {
                        GAShippingAddressList.shared.saveShippingAddessData(arrayProducts: response)
                    }
                    self.tableviewAddress.reloadData()
                    
                } else {
                    self.view.makeToast(message)
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
}


extension GAProfileViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)

        if textField == textfieldName {
            if textfieldName.text!.count < 0 {
                textfieldName.errorMessage = "Please enter name"
                
            }
            else {
                textfieldName.errorMessage = ""
                self.buttonSave.backgroundColor = GAColors.themeButtonColor
                self.buttonSave.isUserInteractionEnabled = true
                
            }
        } else if textField == textfieldEmail {
            if textfieldEmail.text == "" {
                textfieldEmail.errorMessage = "Please enter email address"
                
            } else if !textfieldEmail.isValidEmail(testStr: textfieldEmail.text!) {
                textfieldEmail.errorMessage = "Please enter valid email address"
                
            }
            else {
                textfieldEmail.errorMessage = ""
                self.buttonSave.backgroundColor = GAColors.themeButtonColor
                self.buttonSave.isUserInteractionEnabled = true
            }
            
        }  else if textField == textfieldMobileNumber {
            if textfieldMobileNumber.text!.count < 0 {
                textfieldMobileNumber.errorMessage = "Please enter mobile number"
                
            } else if newString.count > 10 {
                return false
                
            }
            else {
                textfieldMobileNumber.errorMessage = ""
                self.buttonSave.backgroundColor = GAColors.themeButtonColor
                self.buttonSave.isUserInteractionEnabled = true
            }
        }
        
        return true
    }
    
    func isValid() -> Bool {
        
        var isValid = Bool()
        
        if textfieldName.text == "" {
            textfieldName.errorMessage = "Please enter name"
            isValid = false
            
        } else if textfieldEmail.text == "" {
            textfieldEmail.errorMessage = "Please enter email address"
            isValid = false
            
        } else if textfieldMobileNumber.text == ""  {
            textfieldMobileNumber.errorMessage = "Please enter mobile number"
            isValid = false
            
        } else if textfieldMobileNumber.text!.count < 10  {
            textfieldMobileNumber.errorMessage = "Mobile number must be of 10 digits long"
            isValid = false
            
        }  else {
            isValid = true
        }
        
        return isValid
    }
    
}
 

