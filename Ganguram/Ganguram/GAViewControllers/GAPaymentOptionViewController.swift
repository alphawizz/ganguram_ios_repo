//
//  GAPaymentOptionViewController.swift
//  Ganguram
//
//  Created by Apple on 21/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import KRProgressHUD

class GAPaymentOptionViewController: UIViewController {
    
    var merchantKey = String()
    var transactionId = String()
    var amount = String()
    var email = String()
    var invoiceID = Int()
    var environment = String()
    var isKeyboardVisible = false
    var userCredential = String()
    let paymentParams = PayUModelPaymentParams()
    var surepayCount = String()
    let webServiceResponse = PayUWebViewResponse()
    var salt = String()
    let activityIndicator = ActivityIndicator()
    
    let checkoutStoryboardID = "PUUIMainStoryBoard"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //  setupGestureOnContentView()
        //  setupKeyboardHandlers()
        addPaymentResponseNotification()
    }
    
    @IBAction func onPayNow(_ sender: Any) {
        checkoutButtonClicked()
    }
    
    @IBAction func onPayPal(_ sender: Any) {
        
    }
    
    @IBAction func onGogglePay(_ sender: Any) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupInitialValues()
    }
    
    func setupInitialValues() {
        
        merchantKey = "51t571"
        email = GAUser.shared.loginInfoObj.userEmail!
        transactionId = randomString(length: 6)
        amount = "1"
        environment = ENVIRONMENT_PRODUCTION
        userCredential = "merchantKey:uniqueUserIDAtMerchantEnd"
        surepayCount = "2"
        salt = "F10vXyTQ"
    }
    
    func checkoutButtonClicked() {
        
        //Dismiss keyboard
        //  contentView.endEditing(true)
        
        // MARK: - Step 1 -- Set Payment Parameters -
        setPaymentParams()
        
        weak var weakSelf = self
        
        // MARK:  Step 2 -- Generate hashes -
        //    if saltSwitch.isOn {
        //You must never calculate hash locally. It is a big secruity risk. Below code is for demonstration purpose only.
        let vulnerableClass = PayUDontUseThisClass()
        activityIndicator.startActivityIndicatorOn(self.view)
        
        vulnerableClass.getPayUHashes(withPaymentParam: paymentParams, merchantSalt: salt) { (allHashes, hashString, errorMessage) in
            weakSelf?.activityIndicator.stopActivityIndicator()
            hashGenerated(hashes: allHashes, withError: errorMessage)
        }
        // }
        
        
        //        else {
        //            activityIndicator.startActivityIndicatorOn(self.view)
        //            //Calculate hash on your server
        //            PUSAHelperClass.generateHash(fromServer: paymentParams) { (allHashes, errorString) in
        //                weakSelf?.activityIndicator.stopActivityIndicator()
        //                hashGenerated(hashes: allHashes, withError: errorString)
        //            }
        //        }
        
        func hashGenerated(hashes: PayUModelHashes?, withError error:String?) {
            if let err = error {
                print("Error occured in generating hash: \(err)")
            } else {
                weakSelf?.saveHashesAndProceedToFetchPaymentRelatedDetails(hashes)
            }
        }
    }
    
    //Set Payment Parameters
    func setPaymentParams() {
        paymentParams.key = merchantKey
        paymentParams.transactionID = transactionId
        paymentParams.amount = amount
        paymentParams.firstName = GAUser.shared.loginInfoObj.userFullname
        paymentParams.email = email
        paymentParams.phoneNumber = GAUser.shared.loginInfoObj.userPhone
        paymentParams.environment = environment
        paymentParams.udf1 = ""
        paymentParams.udf2 = ""
        paymentParams.udf3 = ""
        paymentParams.udf4 = ""
        paymentParams.udf5 = ""
        paymentParams.userCredentials = userCredential
        paymentParams.surl = "https://payu.herokuapp.com/ios_success"
        paymentParams.furl = "https://payu.herokuapp.com/ios_failure"
        paymentParams.productInfo = "iPhoneXS" //Add information about the production for which transaction is being initiated
    }
    
    // MARK: - Step 3 -- Fetch available payment methods -
    func saveHashesAndProceedToFetchPaymentRelatedDetails(_ hashes: PayUModelHashes?) {
        paymentParams.hashes = hashes
        let webserviceResposne = PayUWebServiceResponse()
        weak var weakSelf = self
        activityIndicator.startActivityIndicatorOn(self.view)
        
        webserviceResposne.getPayUPaymentRelatedDetail(forMobileSDK: paymentParams) { (paymentRelatedDetails, errorMessage, extraParam) in
            weakSelf?.activityIndicator.stopActivityIndicator()
            if let wself = weakSelf {
                if let error = errorMessage {
                    print("Error occured in fetching payment related details: \(error)")
                    return
                } else {
                    
                    // MARK: - Step 4 -- Show PayU's checkout UI -
                    let checkoutStoryBoard = UIStoryboard.init(name: wself.checkoutStoryboardID, bundle: nil)
                    let checkoutInitialVC = checkoutStoryBoard.instantiateViewController(withIdentifier: VC_IDENTIFIER_PAYMENT_OPTION) as! PUUIPaymentOptionVC
                    checkoutInitialVC.paymentParam = wself.paymentParams
                    
                    checkoutInitialVC.surePayCount = Int(wself.surepayCount)!
                    
                    checkoutInitialVC.paymentRelatedDetail = paymentRelatedDetails
                    wself.navigationController?.pushViewController(checkoutInitialVC, animated: true)
                }
            }
        }
    }
    
    // MARK: Response handling -
    
    @objc func responseReceived(notification: NSNotification) {
        
        let notificationObj = notification.object as? Dictionary<String, AnyObject>
        if let dict = notificationObj {
            if let merchantResponse = dict["merchantResponse"] {
                print("Merchant Response :\n \(merchantResponse)")
                self.callApiAfterPaymentConfirmation()
            }
            if let payuResponse = dict["payUResponse"] {
                print("PayU Response :\n \(payuResponse)")
                self.callApiAfterPaymentConfirmation()
            }
        } else {
            print("Response not received")
        }
    }
    
    // MARK: Helper methods -
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    func addPaymentResponseNotification() {
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.responseReceived),
            name: NSNotification.Name(rawValue: kPUUINotiPaymentResponse),
            object: nil)
    }
    
    func callApiAfterPaymentConfirmation() {

        if Connectivity.isConnectedToInternet() {

                   let param : [String : Any] = [ "device_id" : AppDelegate.FirebaseToken,
                                                 "user_id" : GAUser.shared.loginInfoObj.userId!,
                                                 "payment_method" : "payumoney",
                                                 "invoice_id" : invoiceID]

                   KRProgressHUD.show()

                   GAService.shared.apiForConfirmPayment(param: param) { (isSucess, message, response) in

                       KRProgressHUD.dismiss()

                       if isSucess! {
                           self.view.makeToast("Payment successfully completed !!!")
                        self.navigationController?.popToRootViewController(animated: true)
                       } else {
                           self.view.makeToast(message)
                       }
                   }
               } else {
                   self.view.makeToast(GAMessage.networkError)
               }
    }
    
    
    // MARK: - UI Stuff -
    
    
    //    func setupKeyboardHandlers() {
    //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
    //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    //    }
    //
    //    @objc func keyboardWillShow(notification: NSNotification) {
    //        if navigationController?.topViewController?.isKind(of: OrderPageVC.self) == false {
    //            return
    //        }
    //
    //        if isKeyboardVisible {
    //            return
    //        }
    //
    //        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
    //            weak var weakSelf = self
    //            UIView.animate(withDuration: notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double) {
    //                weakSelf?.scrollViewHeight.constant -= keyboardSize.size.height
    //                weakSelf?.checkoutBottom.constant += keyboardSize.size.height
    //                weakSelf?.saltTfToSuperViewBottom.priority = UILayoutPriority(999)
    //                weakSelf?.view.layoutIfNeeded()
    //            }
    //        }
    //    }
    //
    //    @objc func keyboardWillHide(notification: NSNotification)  {
    //        if navigationController?.topViewController?.isKind(of: OrderPageVC.self) == false {
    //            return
    //        }
    //
    //        if isKeyboardVisible == false {
    //            return
    //        }
    //
    //        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
    //            weak var weakSelf = self
    //            UIView.animate(withDuration: notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double) {
    //                weakSelf?.scrollViewHeight.constant += keyboardSize.size.height
    //                weakSelf?.checkoutBottom.constant -= keyboardSize.size.height
    //                weakSelf?.saltTfToSuperViewBottom.priority = UILayoutPriority(700)
    //                weakSelf?.view.layoutIfNeeded()
    //            }
    //        }
    //    }
    //
    //    @objc func keyboardDidShow(notification: NSNotification)  {
    //        isKeyboardVisible = true
    //    }
    //
    //    @objc func keyboardDidHide(notification: NSNotification)  {
    //        isKeyboardVisible = false
    //    }
    //
    //    func setupGestureOnContentView () {
    //        let tapGesture = UITapGestureRecognizer(target: self, action:  #selector (self.contentViewTapped (_:)))
    //
    //        contentView.addGestureRecognizer(tapGesture)
    //    }
    //
    //    @objc func contentViewTapped(_ sender:UITapGestureRecognizer) {
    //        contentView.endEditing(true)
    //    }
    
}




















//    func continueWithCardPayment() {
//
//        let paymentParam = PUMTxnParam()
//      //  paymentParam.key = "qYT2r5E0"
//     //   paymentParam.merchantid = "6917323"
//
//        // client ----
//        paymentParam.key = "51t571"
//        paymentParam.merchantid = "25858"
//        paymentParam.txnID = "txnID123"
//        paymentParam.phone = "9830096322"
//        paymentParam.amount = "1"
//        paymentParam.productInfo = "Nokia"
//        paymentParam.surl = "https://test.payumoney.com/mobileapp/payumoney/success.php"
//        paymentParam.furl = "https://test.payumoney.com/mobileapp/payumoney/failure.php"
//        paymentParam.firstname = "Sweet App"
//        paymentParam.email = "rahulganguram@gmail.com"
//        paymentParam.environment = PUMEnvironment.production
//        paymentParam.udf1 = ""
//        paymentParam.udf2 = ""
//        paymentParam.udf3 = ""
//        paymentParam.udf4 = ""
//        paymentParam.udf5 = ""
//        paymentParam.udf6 = ""
//        paymentParam.udf7 = ""
//        paymentParam.udf8 = ""
//        paymentParam.udf9 = ""
//        paymentParam.udf10 = ""
//        paymentParam.hashValue = getHashValue(txnParam :paymentParam)
//
//        PlugNPlay.presentPaymentViewController(withTxnParams: paymentParam, on: self, withCompletionBlock: { paymentResponse, error, extraParam in
//            if error != nil {
//
//                debugPrint(error?.localizedDescription)
//
//            } else {
//                var message = ""
//                if paymentResponse?["result"] != nil && (paymentResponse?["result"] is [AnyHashable : Any]) {
//                    print(paymentResponse!)
//
//                    self.callApiAfterPaymentConfirmation()
//                    message = "Hello"
//                    //                    message = paymentResponse?["result"]?["error_Message"] as? String ?? ""
//                    //                    if message.isEqual(NSNull()) || message.count == 0 || (message == "No Error") {
//                    //                        message = paymentResponse?["result"]?["status"] as? String ?? ""
//                    //                    }
//                } else {
//                    message = paymentResponse?["status"] as? String ?? ""
//                }
//                //UIUtility.toastMessage(onScreen: message)
//            }
//        })
//    }
//
//          func getHashValue(txnParam :PUMTxnParam)-> String {
//
//          //  let salt = "UlJI8wpReq"
//
//              // client ----
//           let salt = "F10vXyTQ"
//
//              let hashSequence = String(format: "%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@", txnParam.key,txnParam.txnID,txnParam.amount,txnParam.productInfo,txnParam.firstname,txnParam.email,txnParam.udf1,txnParam.udf2,txnParam.udf3,txnParam.udf4,txnParam.udf5,txnParam.udf6,txnParam.udf7,txnParam.udf8,txnParam.udf9,txnParam.udf10, salt)
//
//              return hashSequence.sha512()
//
//          }
//
    
//    }
//
//
//extension String {
//    func sha512() -> String {
//        let data = Data(self.utf8)
//        var digest = [UInt8](repeating: 0, count:Int(CC_SHA512_DIGEST_LENGTH))
//        data.withUnsafeBytes {
//            _ = CC_SHA512($0.baseAddress, CC_LONG(data.count), &digest)
//        }
//        let hexBytes = digest.map { String(format: "%02hhx", $0)}
//
//        return hexBytes.joined().replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "").replacingOccurrences(of: " ", with: "")
//    }

