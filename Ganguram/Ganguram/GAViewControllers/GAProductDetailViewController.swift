//
//  GAProductDetailViewController.swift
//  Ganguram
//
//  Created by Apple on 13/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import KRProgressHUD

class GAProductDetailViewController: UIViewController {

    @IBOutlet weak var imageviewProduct: UIImageView!
    @IBOutlet weak var buttonWishList: UIButton!
    @IBOutlet weak var viewRating: FloatRatingView!
    @IBOutlet weak var labelProductDescription: UILabel!
    @IBOutlet weak var labelProductName: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelTotalPrice: UILabel!
    @IBOutlet weak var buttonAddToCard: GADesignableButton!
    @IBOutlet weak var tableviewReviewList: UITableView!
    
    var dictProdustDetail : ProductDetail!
    var dictSearchedProduct : SearchProductDetail!
    var isAlreadyAddedInCart = Bool()
    var isFromSearchList = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerxib()
        tableviewReviewList.estimatedRowHeight = 93
        tableviewReviewList.rowHeight = UITableView.automaticDimension
        tableviewReviewList.tableFooterView = UIView()

        loadProductData()
        getAllReviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tableviewReviewList.reloadData()
    }
    
    func registerxib()  {
        
        tableviewReviewList.register(UINib.init(nibName: String(describing: GAReviewTableviewCellTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: GAReviewTableviewCellTableViewCell.self))
        
        tableviewReviewList.register(UINib.init(nibName: String(describing: GANoDataTableviewCellTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: GANoDataTableviewCellTableViewCell.self))
    }
    
    func loadProductData()  {
        
        self.labelProductName.text = dictProdustDetail!.productName
        
        self.imageviewProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageviewProduct.sd_setImage(with: URL.init(string: dictProdustDetail!.productImage!)) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                self.imageviewProduct.image = UIImage(named: "imageload")
            } else {
                // Successful in loading image
                self.imageviewProduct.image = image
            }
        }
        
        self.labelPrice.text = "\(dictProdustDetail!.price!)rs"
        self.labelTotalPrice.text = "\(dictProdustDetail!.price!)rs"
        self.viewRating.rating = Double(dictProdustDetail!.rating!)
        self.labelProductDescription.text = dictProdustDetail.productDescription
        
        if dictProdustDetail.wishlist_status! {
           self.buttonWishList.setBackgroundImage(UIImage.init(named: "wishlistRed"), for: .normal)

        } else {
            self.buttonWishList.setBackgroundImage(UIImage.init(named: "blackheart"), for: .normal)
        }
        
        if isAlreadyAddedInCart {
            self.buttonAddToCard.setTitle("Go To Cart", for: .normal)

        } else {
            self.buttonAddToCard.setTitle("Add To Cart", for: .normal)
        }
        
    }
    
    

    @IBAction func onAddToCard(_ sender: UIButton) {
        
        if isAlreadyAddedInCart {
            let popupVC = UIStoryboard(name: "Main", bundle: nil)
            let cardVC = popupVC.instantiateViewController(identifier: String.init(describing: GACardListViewController.self)) as! GACardListViewController
            self.navigationController?.pushViewController(cardVC, animated: true)

        } else {
           addProductInAddToCard()
        }
    }
    
    func addProductInAddToCard()  {
                
        var arrayAddCardList = [ProductDetail]()
        
                   if let objects = UserDefaults.standard.value(forKey: GAUserKey.AddToCard) as? Data {
                      let decoder = JSONDecoder()
                      if let cardList = try? decoder.decode(Array.self, from: objects) as [ProductDetail] {
                        arrayAddCardList = cardList
                        
                        // check Product already exist or not

                        let alreadyExistInCard = arrayAddCardList.filter({$0.productId == dictProdustDetail.productId})
                                     
                                        if alreadyExistInCard.count == 0 {
                                            
                                            dictProdustDetail.dateAdded = Date()
                                            arrayAddCardList.append(dictProdustDetail)
                                           
                                           let encoder = JSONEncoder()
                                           if let encoded = try? encoder.encode(arrayAddCardList){
                                              UserDefaults.standard.set(encoded, forKey: GAUserKey.AddToCard)
                                            self.view.makeToast("Product added in card")
                                            self.buttonAddToCard.setTitle("Go To Cart", for: .normal)
                                            isAlreadyAddedInCart = true
                                           }
                               }
                      }
                   } else {
                    
                    // Add Product first time
                    dictProdustDetail.dateAdded = Date()
                    arrayAddCardList.append(dictProdustDetail)
                    
                    let encoder = JSONEncoder()
                    if let encoded = try? encoder.encode(arrayAddCardList){
                       UserDefaults.standard.set(encoded, forKey: GAUserKey.AddToCard)
                        self.view.makeToast("Product added in card")
                        self.buttonAddToCard.setTitle("Go To Cart", for: .normal)
                        isAlreadyAddedInCart = true

                    }
            }
    }
    
    @IBAction func onAddReview(_ sender: Any) {
        
        let popupVC = UIStoryboard(name: "PopUpStoryboard", bundle: nil)
        let reviewVC = popupVC.instantiateViewController(identifier: String.init(describing: GAAddReviewViewController.self)) as! GAAddReviewViewController
        reviewVC.productID = dictProdustDetail!.productId!
        reviewVC.productDetail = dictProdustDetail!
        self.navigationController?.pushViewController(reviewVC, animated: true)
    }
    
    func getAllReviews()  {
        
        if Connectivity.isConnectedToInternet() {
            
            let param : [String : Any] = ["product_id" : dictProdustDetail!.productId!]
            
            KRProgressHUD.show()
            
            GAService.shared.apiForGetReviews(param: param) { (isSucess, message, response) in
                
                KRProgressHUD.dismiss()
                
                if isSucess! {
                    if let response = response!["Products"] as? [[String : Any]] {
                        GAReviewList.shared.saveReviewData(arrayProducts: response)
                    }
                        self.tableviewReviewList.reloadData()
                } else {
                    self.view.makeToast(message)
                    GAReviewList.shared.arrayReviewData.removeAll()
                        self.tableviewReviewList.reloadData()
                }
            }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
}
