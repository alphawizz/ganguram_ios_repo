//
//  GASignupViewController.swift
//  Ganguram
//
//  Created by Apple on 10/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KRProgressHUD

class GASignupViewController: UIViewController, FSCalendarPopupViewControllerDelegate {
    
    func getAllSelectedDatedFromCalendar(arrDates: [Date]) {
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/YYYY"
        let newStrDate = dateFormatterGet.string(from: arrDates.first!)
        
        textfieldDateOfBirth.text = newStrDate
    }
    
    @IBOutlet weak var textfieldUserName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldDateOfBirth: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldUserCity: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldConfirmPassword: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        textfieldUserName.delegate = self
        textfieldEmail.delegate = self
        textfieldPhoneNumber.delegate = self
        textfieldDateOfBirth.delegate = self
        textfieldUserCity.delegate = self
        textfieldPassword.delegate = self
        textfieldConfirmPassword.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func onSignUp(_ sender: Any) {
        
        if isValid() {
        self.callSignupAPI()
        }
    }
    
    
    @IBAction func onLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func callSignupAPI() {
        
        if Connectivity.isConnectedToInternet() {

        let dictParam : [String : Any] = ["user_fullname" : textfieldUserName.text!,
                                          "user_email" : textfieldEmail.text!,
                                          "user_phone" : textfieldPhoneNumber.text!,
                                          "user_bdate" : textfieldDateOfBirth.text!,
                                          "user_city" : textfieldUserCity.text!,
                                          "user_password" : textfieldPassword.text!]
        
          KRProgressHUD.show()
        GAService.shared.apiForUserSignup(param: dictParam) { (isSucess, message, response) in
            
              KRProgressHUD.dismiss()
            if isSucess! {
                UserDefaults.standard.removeObject(forKey: GAUserKey.AddToCard)
                                   UserDefaults.standard.synchronize()
                self.navigateToDashBoardScreen()
                
            } else {
                  self.view.makeToast(message)
            }
        }
        } else {
            self.view.makeToast(GAMessage.networkError)
        }
    }
    
    func navigateToDashBoardScreen()  {
           
           let main = UIStoryboard(name: "Main", bundle: nil)
           
           let objHome = main.instantiateViewController(withIdentifier: "GADashBoardViewController") as! GADashBoardViewController
           
           let navigationController = NavigationController(rootViewController: objHome)
           let mainViewController = MainViewController()
           
           mainViewController.rootViewController = navigationController
           navigationController.navigationBar.isHidden = false
           
           mainViewController.setup(type: UInt(1))
           
           if #available(iOS 10.0, *) {
               mainViewController.leftViewBackgroundBlurEffect = UIBlurEffect(style: .regular)
           } else {
               // Fallback on earlier versions
           }
           self.navigationController?.pushViewController(mainViewController, animated: false)
       }
    
    
    @IBAction func onDateOfBirth(_ sender: Any) {

        let calendarPopup =  UIStoryboard.init(name: "PopUpStoryboard", bundle: nil).instantiateViewController(identifier: String.init(describing: FSCalendarPopupViewController.self)) as! FSCalendarPopupViewController
                 
               calendarPopup.modalPresentationStyle = .overFullScreen
               calendarPopup.delegate = (self as! FSCalendarPopupViewControllerDelegate)
               self.present(calendarPopup, animated: false, completion: nil)
    }
}

extension GASignupViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        if textField == textfieldUserName {
            if textfieldUserName.text == "" {
                textfieldUserName.errorMessage = "Please enter username"
                
            }
            else {
                textfieldUserName.errorMessage = ""
            }
            
        } else if textField == textfieldEmail {
            
            if textfieldEmail.text == "" {
                textfieldEmail.errorMessage = "Please enter email address"
                
            } else if !textfieldEmail.isValidEmail(testStr: textfieldEmail.text!) {
                textfieldEmail.errorMessage = "Please enter valid email address"
                
            }
            else {
                textfieldEmail.errorMessage = ""
            }
            
        } else if textField == textfieldPhoneNumber {
            
            if textfieldPhoneNumber.text == "" {
                textfieldPhoneNumber.errorMessage = "Please enter phone number"
                
            } else if newString.count > 10 {
                return false
            }
            else {
                textfieldPhoneNumber.errorMessage = ""
            }
            
        } else if textField == textfieldDateOfBirth {
            if textfieldDateOfBirth.text!.count < 1 {
                textfieldDateOfBirth.errorMessage = "Please enter date of birth"
            }
            else {
                textfieldDateOfBirth.errorMessage = ""
            }
            
        } else if textField == textfieldUserCity {
            if textfieldUserCity.text!.count < 1 {
                textfieldUserCity.errorMessage = "Please enter user city"
                
            }
            else {
                textfieldUserCity.errorMessage = ""
            }
            
        } else if textField == textfieldPassword {
            if textfieldPassword.text!.count < 1 {
                textfieldPassword.errorMessage = "Please enter password"
                
            }
            else {
                textfieldPassword.errorMessage = ""
            }
            
        } else if textField == textfieldConfirmPassword {
            if textfieldConfirmPassword.text!.count < 1 {
                textfieldConfirmPassword.errorMessage = "Please enter confirm password"
                
            }
            else {
                textfieldConfirmPassword.errorMessage = ""
            }
        }
        else {
            textfieldConfirmPassword.errorMessage = ""
        }
        
        return true
    }
    
    func isValid() -> Bool {
        
        var isValid = Bool()
        
        if textfieldUserName.text == "" {
            textfieldUserName.errorMessage = "Please enter username"
            isValid = false
            
        }else if textfieldEmail.text == "" {
            textfieldEmail.errorMessage = "Please enter email address"
            isValid = false
            
        } else if textfieldPhoneNumber.text == "" {
            textfieldPhoneNumber.errorMessage = "Please enter phone number"
            isValid = false
            
        } else if textfieldDateOfBirth.text == "" {
            textfieldDateOfBirth.errorMessage = "Please enter date of birth"
            isValid = false
            
        } else if textfieldUserCity.text == "" {
            textfieldUserCity.errorMessage = "Please enter user city"
            isValid = false
            
        } else if textfieldPassword.text == ""  {
            textfieldPassword.errorMessage = "Please enter password"
            isValid = false
            
        } else if textfieldConfirmPassword.text == ""  {
            textfieldConfirmPassword.errorMessage = "Please enter confirm password"
            isValid = false
            
        } else if textfieldPassword.text != textfieldConfirmPassword.text {
            textfieldConfirmPassword.errorMessage = "Password and confirm password are mismatch"
            isValid = false
        }
        
        else {
            isValid = true
        }
        
        return isValid
    }
    
}
